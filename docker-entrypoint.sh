#!/usr/bin/env bash
# Used for building official image

set -e

python manage.py migrate --noinput
python manage.py collectstatic --noinput

gunicorn orea.wsgi:application --bind 0.0.0.0:8000
