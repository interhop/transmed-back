import random
import uuid
from datetime import timedelta
from typing import List

from django.utils import timezone
from rest_framework import status

from orea.models import User
from orea.tests_tools import ViewSetTests, new_random_user,\
    CaseRetrieveFilter, ListCase, CreateCase, PatchCase, DeleteCase
from infra.models import Bed, Unit, Hospital, IntensiveCareService as Service
from infra.views import BedViewSet
from users.models import Access

INFRA_URL = "/infra"


class BedTests(ViewSetTests):
    unupdatable_fields = []
    unsettable_default_fields = dict()
    unsettable_fields = []

    objects_url = f"{INFRA_URL}/beds"
    retrieve_view = BedViewSet.as_view({'get': 'retrieve'})
    list_view = BedViewSet.as_view({'get': 'list'})
    create_view = BedViewSet.as_view({'post': 'create'})
    delete_view = BedViewSet.as_view({'delete': 'destroy'})
    update_view = BedViewSet.as_view({'patch': 'partial_update'})
    model = Bed
    model_objects = Bed.objects
    model_fields = Bed._meta.fields

    def setUp(self):
        super(BedTests, self).setUp()

        # Hospitals
        self.hosp1: Hospital = Hospital.objects.create(name="hosp1")
        self.hosp2: Hospital = Hospital.objects.create(name="hosp2")

        # Services
        self.service1: Service = Service.objects.create(name="service1",
                                                        hospital=self.hosp1)
        self.service2: Service = Service.objects.create(name="service2",
                                                        hospital=self.hosp1)
        self.service3: Service = Service.objects.create(name="service3",
                                                        hospital=self.hosp2)

        # Units
        self.unit_serv_1: Unit = Unit.objects.create(
            name="unit1", intensive_care_service=self.service1)
        self.unit_serv_2: Unit = Unit.objects.create(
            name="unit2", intensive_care_service=self.service2)
        self.unit_serv_3: Unit = Unit.objects.create(
            name="unit3", intensive_care_service=self.service3)

        # Users
        self.admin: User = new_random_user("is", "admin", is_staff=True)
        self.not_admin: User = new_random_user("not", "admin")
        self.user_1_serv_1_3_admin: User = new_random_user("admin", "h1")
        # with right_review
        self.access_user_1_serv_1: Access = Access.objects.create(
            intensive_care_service=self.service1,
            user=self.user_1_serv_1_3_admin,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=False,
            right_review=True
        )
        # with right_edit
        self.access_user_1_serv_3: Access = Access.objects.create(
            intensive_care_service=self.service3,
            user=self.user_1_serv_1_3_admin,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=True,
            right_review=False
        )


class BedCaseRetrieveFilter(CaseRetrieveFilter):
    def __init__(self, unit_index: int, exclude: dict = None):
        self.unit_index = unit_index
        super(BedCaseRetrieveFilter, self).__init__(exclude=exclude)


class BedGetListTests(BedTests):
    def setUp(self):
        super(BedGetListTests, self).setUp()
        nb_beds = 500

        self.list_beds: List[Bed] = Bed.objects.bulk_create([
            Bed(
                **{
                    'unit_index': i,
                    'unit': random.choice([
                        self.unit_serv_1, self.unit_serv_2, self.unit_serv_3
                    ]),
                    'usable': random.random() > 0.5,
                }
            ) for i in range(nb_beds)
        ])

    def test_get_without_rights(self):
        # As a user with no right, I cannot get any bed
        case = ListCase(
            to_find=[],
            success=True,
            status=status.HTTP_200_OK,
            user=self.not_admin
        )
        self.check_get_paged_list_case(case)

    def test_get_all_beds_as_admin(self):
        # As a user with is_staff=True, I can get all beds
        case = ListCase(
            to_find=self.list_beds,
            success=True,
            status=status.HTTP_200_OK,
            user=self.admin
        )
        self.check_get_paged_list_case(case)

    def test_get_service_beds(self):
        # As a user with access to a service, I can get this service's beds
        # (access with right_review or not)
        case = ListCase(
            to_find=[b for b in self.list_beds
                     if b.unit.intensive_care_service.pk
                     in [self.service1.pk, self.service3.pk]],
            success=True,
            status=status.HTTP_200_OK,
            user=self.user_1_serv_1_3_admin
        )
        self.check_get_paged_list_case(case)

    def test_get_list_with_params_1(self):
        # As a user with is_staff=True, I can get beds filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"search={self.unit_serv_1.name}",
                to_find=[
                    bed for bed in self.list_beds
                    if bed.unit.pk == self.unit_serv_1.pk],
                params=dict(search=self.unit_serv_1.name)
            )
        )

    def test_get_list_with_params_2(self):
        # As a user with is_staff=True, I can get beds filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"search={self.service1.name}",
                to_find=[
                    bed for bed in self.list_beds
                    if bed.unit.intensive_care_service.pk == self.service1.pk],
                params=dict(search=self.service1.name)
            )
        )

    def test_get_list_with_params_3(self):
        # As a user with is_staff=True, I can get beds filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"search={self.hosp1.name}",
                to_find=[
                    bed for bed in self.list_beds
                    if (bed.unit.intensive_care_service.hospital.pk
                        == self.hosp1.pk)],
                params=dict(search=self.hosp1.name)
            )
        )

    def test_get_list_with_params_4(self):
        # As a user with is_staff=True, I can get beds filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title=f"unit={self.unit_serv_1.pk}",
                to_find=[
                    bed for bed in self.list_beds
                    if (bed.unit.pk == self.unit_serv_1.pk)],
                params=dict(unit=self.unit_serv_1.pk)
            )
        )

    def test_get_list_with_params_5(self):
        # As a user with is_staff=True, I can get beds filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title="unit_index=5",
                to_find=[bed for bed in self.list_beds if bed.unit_index == 5],
                params=dict(unit_index=5)
            )
        )

    def test_get_list_with_params_6(self):
        # As a user with is_staff=True, I can get beds filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin)
        self.check_get_paged_list_case(ListCase(
                **basic_case_dict,
                title="usable=True",
                to_find=[bed for bed in self.list_beds if bed.usable],
                params=dict(usable=True)
            )
        )

    def test_error_not_authenticated(self):
        # As a user not authenticated, I cannot get any beds
        case = ListCase(
            to_find=[],
            success=False,
            status=status.HTTP_401_UNAUTHORIZED
        )
        self.check_get_paged_list_case(case)


class BedCreateTests(BedTests):
    def setUp(self):
        super(BedCreateTests, self).setUp()

        self.creation_data = {
            'unit_index': 1,
            'unit': str(self.unit_serv_3.pk),
            'usable': True
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=BedCaseRetrieveFilter(unit_index=1),
            user=None, status=None, success=None,
        )

    def test_create_as_admin(self):
        # As a user with is_staff=True, I can create a new bed
        case = self.basic_create_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_201_CREATED,
        )
        self.check_create_case(case)

    def test_error_create_wrong_data(self):
        # As a user with is_staff=False, I cannot create a new bed
        cases = [self.basic_create_case.clone(
            user=self.not_admin,
            data=data,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        ) for data in [
            {**self.creation_data, 'unit': str(uuid.uuid4())},
        ]]
        [self.check_create_case(case) for case in cases]

    def test_error_create_as_basic_user(self):
        # As a user with is_staff=False, I cannot create a new bed
        case = self.basic_create_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_create_case(case)

    # todo : see later if relevant to prevent
    def test_error_create_as_service_admin(self):
        # As a user with right_review access on a service,
        # I cannot create a new bed to this service
        case = self.basic_create_case.clone(
            user=self.user_1_serv_1_3_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_create_case(case)


class BedPatchTests(BedTests):
    def setUp(self):
        super(BedPatchTests, self).setUp()

        self.created_data = {
            'unit_index': 1,
            'unit': self.unit_serv_3,
            'usable': True
        }
        self.base_data_to_update = {
            'unit_index': 2,
            'unit': str(self.unit_serv_1.pk),
            'usable': False
        }
        self.basic_patch_case = PatchCase(
            initial_data=self.created_data,
            data_to_update=self.base_data_to_update,
            user=None, status=None, success=None,
        )

    def test_patch_as_admin(self):
        # As a user with is_staff=True, I can edit a bed
        case = self.basic_patch_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_200_OK,
        )
        self.check_patch_case(case)

    def test_error_patch_as_basic_user(self):
        # As a user with is_staff=False, I cannot edit a bed
        case = self.basic_patch_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_patch_case(case)

    # todo : see later if relevant to prevent
    def test_error_patch_as_services_admin(self):
        # As a user with access admin of right_review on both services,
        # I cannot edit a bed from one of this service to another

        # complete user_1 access to service_1 for showing no ability to update
        self.access_user_1_serv_1.right_review = True
        self.access_user_1_serv_1.save()

        case = self.basic_patch_case.clone(
            user=self.user_1_serv_1_3_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_patch_case(case)


class BedDeleteTests(BedTests):
    def setUp(self):
        super(BedDeleteTests, self).setUp()

        self.created_data = {
            'unit_index': 1,
            'unit': self.unit_serv_3,
            'usable': True
        }
        self.basic_delete_case = DeleteCase(
            data_to_delete=self.created_data,
            user=None, status=None, success=None,
        )

    # when we'll be safe with bed deletion (cascade, deletion, etc.)
    def test_delete_user_as_main_admin(self):
        # As a user with is_staff=True, I can soft-delete a bed
        case = self.basic_delete_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_204_NO_CONTENT,
        )
        self.check_delete_case(case)

    def test_error_delete_user_as_simple_user(self):
        # As a user with is_staff=False, I cannot delete a bed
        case = self.basic_delete_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_delete_case(case)

    # todo : see later if relevant to prevent
    def test_error_delete_as_service_admin(self):
        # As a user with right_review access on a service,
        # I cannot delete a bed from this service
        case = self.basic_delete_case.clone(
            user=self.user_1_serv_1_3_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_delete_case(case)
