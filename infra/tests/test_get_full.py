import datetime
from datetime import timedelta
from typing import List, Dict, Union

from django.db.models import Model
from django.utils import timezone
from django.utils.datetime_safe import date
from djangorestframework_camel_case.util import camelize
from rest_framework import status
from rest_framework.test import force_authenticate

from orea.models import User
from orea.tests_tools import new_random_user, PagedResponse, BaseTests
from infra.models import Bed, Unit, Hospital, IntensiveCareService as Service
from infra.views import BedViewSet, UnitViewSet, IntensiveCareServiceViewSet
from orea.utils import prettify_dict
from users.models import Access

INFRA_URL = "/infra"


def model_base_dict(m: Model) -> Dict:
    return dict(
        (f.name, getattr(getattr(m, f.name), 'pk', getattr(m, f.name)))
        for f in m._meta.fields
    )


class GetFullTests(BaseTests):
    def setUp(self):
        super(GetFullTests, self).setUp()

        # Infra
        self.hosp1: Hospital = Hospital.objects.create(name="hosp1")
        self.service: Service = Service.objects.create(name="service",
                                                       hospital=self.hosp1)
        self.unit: Unit = Unit.objects.create(
            name="unit1", intensive_care_service=self.service)
        self.bed_1: Bed = Bed.objects.create(
            unit_index=1, unit=self.unit, usable=True)
        self.bed_2: Bed = Bed.objects.create(
            unit_index=2, unit=self.unit, usable=True)

        # Users
        # self.admin: User = new_random_user("is", "admin", is_staff=True)
        self.user_1: User = new_random_user("admin", "h1")
        # with right_review
        self.access: Access = Access.objects.create(
            intensive_care_service=self.service,
            user=self.user_1,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=False,
            right_review=True
        )

        # Patients
        from patients.models import Patient, PatientStay, BedStay,\
            FailureMeasure, DetectionMeasure, PatientNote
        self.test_str = "test"
        self.test_birth = date.fromisoformat("2000-01-01")
        self.test_date_1 = timezone.now() - timedelta(days=2)
        self.test_date_2 = timezone.now() - timedelta(days=1)
        self.test_date_3 = timezone.now()
        self.test_json = '{"key": "' + self.test_str + '"}'
        self.test_int = 100
        self.test_choice_index = 0
        self.patient_1: Patient = Patient.objects.create(
            local_id=self.test_str,
            first_name=self.test_str,
            last_name=self.test_str,
            birth_date=self.test_birth,
            sex=Patient.PatientSex.values[self.test_choice_index],
            size_cm=self.test_int,
            weight_kg=self.test_int,
            antecedents=self.test_json,
            allergies=self.test_json,
        )
        self.stay: PatientStay = PatientStay.objects.create(
            patient=self.patient_1,
            created_by=self.user_1,
            hospitalisation_cause=self.test_str,
            severity=PatientStay.StaySeverity.values[self.test_choice_index]
        )
        BedStay.objects.create(
            start_date=self.test_date_1.date(),
            end_date=self.test_date_2.date(),
            bed=self.bed_2,
            patient_stay=self.stay,
        )
        self.bed_stay: BedStay = BedStay.objects.create(
            start_date=self.test_date_2.date(),
            end_date=None,
            bed=self.bed_1,
            patient_stay=self.stay,
        )
        patient_1_fm: FailureMeasure = FailureMeasure.objects.create(
            failure_type=(
                FailureMeasure.FailureType.values[self.test_choice_index]
            ),
            active=True,
            indication=self.test_str,
            stay=self.stay,
            created_by=self.user_1,
            measure_datetime=self.test_date_3,
        )
        patient_1_det: DetectionMeasure = DetectionMeasure.objects.create(
            blse=True,
            sarm=True,
            bhc=True,
            clostridium=True,
            stay=self.stay,
            created_by=self.user_1,
            measure_datetime=self.test_date_3,
        )
        PatientNote.objects.create(
            created_by=self.user_1,
            stay=self.stay,
            type=PatientNote.NoteType.todo_list,
            content=self.test_str,
        )

        # Results expected
        self.full_stay_result = dict(
            created_by=self.user_1.pk,
            patient={
                **model_base_dict(self.patient_1),
                'current_stay': self.stay.pk,
                # allergies=self.test_json,
                # antecedents=self.test_json,
                # size_cm=self.test_int,
                # weight_kg=self.test_int,
            },
            failure_measures=[
                model_base_dict(patient_1_fm),
                # dict(
                # stay=self.stay.pk,
                # created_by=self.user_1.pk,
                # measure_datetime=self.test_date_3,
                # failure_type=(
                #     FailureMeasure.FailureType
                #     .values[self.test_choice_index]
                # ),
                # active=True,
                # indication=self.test_str,
            ],
            detections=[
                model_base_dict(patient_1_det),
                # dict(
                # stay=self.stay.pk,
                # created_by=self.user_1.pk,
                # measure_datetime=self.test_date_3,
                # blse=True,
                # sarm=True,
                # bhc=True,
                # clostridium=True,
            ],
            todo_list=dict(
                created_by=self.user_1.pk,
                stay=self.stay.pk,
                type=PatientNote.NoteType.todo_list.value,
                content=self.test_str,
            ),
            start_date=datetime.datetime.fromisoformat(
                self.test_date_1.date().isoformat()
            ).astimezone(),
            end_date=None,
        )
        self.full_beds_result = {
            self.bed_1.pk.__str__(): {
                **model_base_dict(self.bed_1),
                'bed_description': self.bed_1.bed_description,
                'current_patient_stay': self.full_stay_result,
            },
            self.bed_2.pk.__str__(): {
                **model_base_dict(self.bed_2),
                'bed_description': self.bed_2.bed_description,
                'current_patient_stay': None,
            },
        }
        self.full_units_result = {
            self.unit.pk.__str__(): {
                **model_base_dict(self.unit),
                'beds': list(self.full_beds_result.values()),
            }
        }

        self.full_services_result = {
            self.service.pk.__str__(): {
                **model_base_dict(self.service),
                'hospital': self.hosp1.name,
                'units': list(self.full_units_result.values())
            }
        }

    def check_full_result(self, expected: Dict, received: List):
        def report_msg(context: str, msg: str) -> str:
            return f"In context {context}, {msg}"

        def get_received_value(v: any) -> Union[str, int]:
            if isinstance(v, datetime.datetime):
                return v.isoformat().replace('+00:00', 'Z')
            if isinstance(v, int):
                return v
            if v is None:
                return v
            return str(v)

        def list_equal(exp: List, rec: List, context: str = ""):
            self.assertEqual(len(exp), len(rec))
            if not len(exp):
                return
            if isinstance(exp[0], list):
                raise Exception(report_msg(
                    context,
                    "expected member of list is a list,"
                    "comparison is not provided for this case.")
                )
            elif isinstance(exp[0], dict):
                self.assertTrue(
                    isinstance(rec[0], dict),
                    report_msg(context, "received value was not a list of "
                                        "objects as expected.")
                )
                for exp_item in exp:
                    exp_id = exp_item.get('uuid', None)
                    self.assertIsNotNone(
                        exp_id,
                        report_msg(context, "expected item has no uuid.")
                    )
                    exp_id = str(exp_id)
                    matches = list(
                        rec_item for rec_item in rec
                        if rec_item.get('uuid') == exp_id)
                    self.assertEqual(
                        len(matches), 1,
                        report_msg(context, f"received list did not contain"
                                            f" one object {exp_id}")
                    )
                    rec_item = matches[0]
                    dict_equal(exp_item, rec_item, f"{context}:{exp_id}")
            else:
                self.assertCountEqual(
                    exp, rec,
                    report_msg(context, "list received is not expected")
                )

        def dict_equal(exp: Dict, rec: Dict, context: str = ""):
            for f, exp_val in exp.items():
                self.assertTrue(
                    f in rec, report_msg(context, f"field {f} was not received."
                                                  f" {prettify_dict(rec)}")
                )
                rec_val = rec.get(f)
                if isinstance(exp_val, dict):
                    self.assertTrue(
                        isinstance(rec_val, dict),
                        report_msg(
                            context, f"expected value at {f} "
                                     f"{rec_val}) should be a dict"
                        )
                    )
                    dict_equal(exp_val, rec_val, f"{context}.{f}")
                elif isinstance(exp_val, list):
                    self.assertTrue(
                        isinstance(rec_val, list),
                        report_msg(
                            context, f"expected value at {f} ({rec_val}) "
                                     f"should be a list"
                        )
                    )
                    list_equal(exp_val, rec_val, f"{context}.{f}")
                else:
                    self.assertEqual(
                        get_received_value(exp_val), rec_val,
                        report_msg(
                            context, f"received value at {f} "
                                     f"not expected."
                        )
                    )

        self.assertTrue(
            isinstance(received, list),
            f"Expected a list, received {received.__class__}"
        )

        for rec in received:
            exp: Dict = expected.get(rec['uuid'])
            self.assertIsNotNone(exp, f"Following item was not expected: "
                                      f"{prettify_dict(rec)}")
            dict_equal(exp, rec, rec['uuid'])

        if len(expected) != len(received):
            for pk, exp in expected.items():
                self.assertTrue(
                    any(rec['pk'] == pk for rec in received),
                    f"Following expected item was not receive: "
                    f"{prettify_dict(exp)}"
                )

    def check_case(self, url: str, view: any, expected: any):
        request = self.factory.get(
            path=url,
            data=dict(full=True)
        )
        force_authenticate(request, self.user_1)
        response = view(request)

        response.render()
        self.assertEqual(
            response.status_code, status.HTTP_200_OK,
            msg=(prettify_dict(response.content) if response.content else ""),
        )

        res = PagedResponse(response)
        self.check_full_result(camelize(expected), res.results)

    def test_get_full_beds(self):
        self.check_case(f"{INFRA_URL}/beds",
                        BedViewSet.as_view({'get': 'list'}),
                        self.full_beds_result)

    def test_get_full_units(self):
        self.check_case(f"{INFRA_URL}/units",
                        UnitViewSet.as_view({'get': 'list'}),
                        self.full_units_result)

    def test_get_full_services(self):
        self.check_case(f"{INFRA_URL}/services",
                        IntensiveCareServiceViewSet.as_view({'get': 'list'}),
                        self.full_services_result)
