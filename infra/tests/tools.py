from __future__ import annotations

from infra.models import Hospital, IntensiveCareService, Unit, Bed
from orea.tests_tools import ViewSetTests


class SimpleInfraSetup:
    hosp = None
    serv_1 = None
    hosp_1_unit_1 = None
    hosp_1_unit_2 = None
    hosp_1_beds = None
    hosp_2 = None
    serv_2 = None
    hosp_2_unit_1 = None
    hosp_2_unit_2 = None
    hosp_2_beds = None

    def setUp(self):
        self.hosp = Hospital.objects.create(name='Balamb')
        self.serv_1 = IntensiveCareService.objects.create(
            name="Seed", hospital=self.hosp)
        self.hosp_1_unit_1 = Unit.objects.create(
            name="Squall", intensive_care_service=self.serv_1)
        self.hosp_1_unit_2 = Unit.objects.create(
            name="Linoa", intensive_care_service=self.serv_1)

        self.hosp_1_beds = Bed.objects.bulk_create(
            Bed(unit=self.hosp_1_unit_1, unit_index=i)
            for i in range(5))
        self.hosp_1_beds += Bed.objects.bulk_create(
            Bed(unit=self.hosp_1_unit_2, unit_index=i)
            for i in range(5))

        self.hosp_2 = Hospital.objects.create(name='Galbadia')
        self.serv_2 = IntensiveCareService.objects.create(
            name="Deling", hospital=self.hosp_2)
        self.hosp_2_unit_1 = Unit.objects.create(
            name="Irvine", intensive_care_service=self.serv_2)
        self.hosp_2_unit_2 = Unit.objects.create(
            name="Selphie", intensive_care_service=self.serv_2)
        self.hosp_2_beds = Bed.objects.bulk_create(
            Bed(unit=self.hosp_2_unit_1, unit_index=i) for i in range(5))
        self.hosp_2_beds += Bed.objects.bulk_create(
            Bed(unit=self.hosp_2_unit_2, unit_index=i) for i in range(5))


class ViewSetTestsWithBasicInfra(ViewSetTests, SimpleInfraSetup):
    def setUp(self):
        super(ViewSetTestsWithBasicInfra, self).setUp()
        SimpleInfraSetup.setUp(self)
