import random
import uuid
from datetime import timedelta
from typing import List

from django.utils import timezone
from rest_framework import status

from orea.models import User
from orea.tests_tools import ViewSetTests, new_random_user,\
    CaseRetrieveFilter, random_str, ListCase, CreateCase, PatchCase, DeleteCase
from infra.models import Unit, Hospital, IntensiveCareService as Service
from infra.views import UnitViewSet
from users.models import Access

INFRA_URL = "/infra"


class UnitTests(ViewSetTests):
    unupdatable_fields = []
    unsettable_default_fields = dict()
    unsettable_fields = []

    objects_url = f"{INFRA_URL}/units"
    retrieve_view = UnitViewSet.as_view({'get': 'retrieve'})
    list_view = UnitViewSet.as_view({'get': 'list'})
    create_view = UnitViewSet.as_view({'post': 'create'})
    delete_view = UnitViewSet.as_view({'delete': 'destroy'})
    update_view = UnitViewSet.as_view({'patch': 'partial_update'})
    model = Unit
    model_objects = Unit.objects
    model_fields = Unit._meta.fields

    def setUp(self):
        super(UnitTests, self).setUp()

        # Hospitals
        self.hosp1: Hospital = Hospital.objects.create(name="hosp1")
        self.hosp2: Hospital = Hospital.objects.create(name="hosp2")

        # Services
        self.service1: Service = Service.objects.create(name="service1",
                                                        hospital=self.hosp1)
        self.service2: Service = Service.objects.create(name="service2",
                                                        hospital=self.hosp1)
        self.service3: Service = Service.objects.create(name="service3",
                                                        hospital=self.hosp2)

        # Users
        self.admin: User = new_random_user("is", "admin", is_staff=True)
        self.not_admin: User = new_random_user("not", "admin")
        self.user_1_serv_1_3_admin: User = new_random_user("admin", "h1")
        # with right_review
        self.access_user_1_serv_1: Access = Access.objects.create(
            intensive_care_service=self.service1,
            user=self.user_1_serv_1_3_admin,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=False,
            right_review=True
        )
        # with right_edit
        self.access_user_1_serv_3: Access = Access.objects.create(
            intensive_care_service=self.service3,
            user=self.user_1_serv_1_3_admin,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=True,
            right_review=False
        )


class UnitCaseRetrieveFilter(CaseRetrieveFilter):
    def __init__(self, name: str, exclude: dict = None):
        self.name = name
        super(UnitCaseRetrieveFilter, self).__init__(exclude=exclude)


class UnitGetListTests(UnitTests):
    def setUp(self):
        super(UnitGetListTests, self).setUp()
        self.name_pattern = random_str(3).replace(' ', 'e')
        nb_units = 500

        self.unit_names = [
                              random_str(random.randint(5, 15)) for _ in
                              range(nb_units - 110)
                          ] + [
                              random_str(random.randint(2, 7))
                              + self.name_pattern
                              + random_str(random.randint(3, 5)) for _ in
                              range(110)
                          ]

        self.list_units: List[Unit] = Unit.objects.bulk_create([
            Unit(
                **{
                    'name': name,
                    'intensive_care_service': random.choice([
                        self.service1, self.service2, self.service3
                    ])
                }
            ) for name in self.unit_names
        ])

    def test_get_no_units(self):
        # As a user with no right, I cannot get any unit
        case = ListCase(
            to_find=[],
            success=True,
            status=status.HTTP_200_OK,
            user=self.not_admin
        )
        self.check_get_paged_list_case(case)

    def test_get_all_units_as_admin(self):
        # As a user with is_staff=True, I can get all units
        case = ListCase(
            to_find=self.list_units,
            success=True,
            status=status.HTTP_200_OK,
            user=self.admin
        )
        self.check_get_paged_list_case(case)

    def test_get_service_units(self):
        # As a user with access to a service, I can get this service's units
        # (access with right_review or not)
        case = ListCase(
            to_find=[u for u in self.list_units if u.intensive_care_service.pk
                     in [self.service1.pk, self.service3.pk]],
            success=True,
            status=status.HTTP_200_OK,
            user=self.user_1_serv_1_3_admin
        )
        self.check_get_paged_list_case(case)

    def test_get_list_with_params(self):
        # As a user with is_staff=True, I can get units filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.admin)
        cases = [
            ListCase(
                **basic_case_dict,
                title=f"search={self.name_pattern}",
                to_find=[
                    unit for unit in self.list_units
                    if self.name_pattern in unit.name],
                params=dict(search=self.name_pattern)
            ),
            ListCase(
                **basic_case_dict,
                title=f"search={self.service1.name}",
                to_find=[
                    unit for unit in self.list_units
                    if (
                            self.service1.name in unit.name or
                            unit.intensive_care_service.pk == self.service1.pk
                    )],
                params=dict(search=self.service1.name)
            ),
            ListCase(
                **basic_case_dict,
                title=f"search={self.hosp1.name}",
                to_find=[
                    unit for unit in self.list_units
                    if (
                            self.hosp1.name in unit.name
                            or (unit.intensive_care_service.hospital.pk
                                == self.hosp1.pk)
                    )],
                params=dict(search=self.hosp1.name)
            ),
            ListCase(
                **basic_case_dict,
                title=f"hospital={self.hosp1.pk}",
                to_find=[
                    unit for unit in self.list_units
                    if unit.intensive_care_service.hospital.pk == self.hosp1.pk
                ],
                params=dict(hospital=self.hosp1.pk)
            ),
            ListCase(
                **basic_case_dict,
                title=f"intensive_care_service={self.service3.pk}",
                to_find=[
                    unit for unit in self.list_units
                    if unit.intensive_care_service.pk == self.service3.pk
                ],
                params=dict(intensive_care_service=self.service3.pk)
            ),
        ]
        [self.check_get_paged_list_case(case) for case in cases]

    def test_error_not_authenticated(self):
        # As a user not authenticated, I cannot get any units
        case = ListCase(
            to_find=[],
            success=False,
            status=status.HTTP_401_UNAUTHORIZED
        )
        self.check_get_paged_list_case(case)


class UnitCreateTests(UnitTests):
    def setUp(self):
        super(UnitCreateTests, self).setUp()

        test_unit_name = random_str(10)
        self.creation_data = {
            'name': test_unit_name,
            'intensive_care_service': str(self.service3.pk)
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=UnitCaseRetrieveFilter(name=test_unit_name.strip()),
            user=None, status=None, success=None,
        )

    def test_create_as_admin(self):
        # As a user with is_staff=True, I can create a new unit
        case = self.basic_create_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_201_CREATED,
        )
        self.check_create_case(case)

    def test_error_create_wrong_data(self):
        # As a user with is_staff=False, I cannot create a new unit
        cases = [self.basic_create_case.clone(
            user=self.not_admin,
            data=data,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        ) for data in [
            {**self.creation_data, 'intensive_care_unit': str(uuid.uuid4())}
        ]]
        [self.check_create_case(case) for case in cases]

    def test_error_create_as_basic_user(self):
        # As a user with is_staff=False, I cannot create a new unit
        case = self.basic_create_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_create_case(case)

    # todo : see later if relevant to prevent
    def test_error_create_as_service_admin(self):
        # As a user with right_review access on a service,
        # I cannot create a new unit to this service
        case = self.basic_create_case.clone(
            user=self.user_1_serv_1_3_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_create_case(case)


class UnitPatchTests(UnitTests):
    def setUp(self):
        super(UnitPatchTests, self).setUp()

        self.created_data = {
            'name': 'created',
            'intensive_care_service': self.service3
        }
        self.base_data_to_update = {
            'name': 'updated',
            'intensive_care_service': str(self.service1.pk)
        }
        self.basic_patch_case = PatchCase(
            initial_data=self.created_data,
            data_to_update=self.base_data_to_update,
            user=None, status=None, success=None,
        )

    def test_patch_as_admin(self):
        # As a user with is_staff=True, I can edit a unit
        case = self.basic_patch_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_200_OK,
        )
        self.check_patch_case(case)

    def test_error_patch_as_basic_user(self):
        # As a user with is_staff=False, I cannot edit a unit
        case = self.basic_patch_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_patch_case(case)

    # todo : see later if relevant to prevent
    def test_error_patch_as_services_admin(self):
        # As a user with access admin of right_review on both services,
        # I cannot edit a unit from one of this service to another

        # complete user_1 acces to service_1 for showing no ability to update
        self.access_user_1_serv_1.right_review = True
        self.access_user_1_serv_1.save()

        case = self.basic_patch_case.clone(
            user=self.user_1_serv_1_3_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_patch_case(case)


class UnitDeleteTests(UnitTests):
    def setUp(self):
        super(UnitDeleteTests, self).setUp()

        self.created_data = {
            'name': 'created',
            'intensive_care_service': self.service3
        }
        self.basic_delete_case = DeleteCase(
            data_to_delete=self.created_data,
            user=None, status=None, success=None,
        )

    # when we'll be safe with unit deletion (cascade, deletion, etc.)
    def test_delete_user_as_main_admin(self):
        # As a user with is_staff=True, I can soft-delete a unit
        case = self.basic_delete_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_204_NO_CONTENT,
        )
        self.check_delete_case(case)

    def test_error_delete_user_as_simple_user(self):
        # As a user with is_staff=False, I cannot delete a unit
        case = self.basic_delete_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_delete_case(case)

    # todo : see later if relevant to prevent
    def test_error_delete_as_service_admin(self):
        # As a user with right_review access on a service,
        # I cannot delete a unit from this service
        case = self.basic_delete_case.clone(
            user=self.user_1_serv_1_3_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_delete_case(case)
