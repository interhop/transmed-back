import random
from typing import List

from rest_framework import status

from orea.models import User
from orea.tests_tools import ViewSetTests, new_random_user,\
    CaseRetrieveFilter, random_str, ListCase, CreateCase, PatchCase, DeleteCase
from infra.models import Hospital
from infra.views import HospitalViewSet

INFRA_URL = "/infra"


class HospitalTests(ViewSetTests):
    unupdatable_fields = []
    unsettable_default_fields = dict()
    unsettable_fields = []

    objects_url = f"{INFRA_URL}/hospitals"
    retrieve_view = HospitalViewSet.as_view({'get': 'retrieve'})
    list_view = HospitalViewSet.as_view({'get': 'list'})
    create_view = HospitalViewSet.as_view({'post': 'create'})
    delete_view = HospitalViewSet.as_view({'delete': 'destroy'})
    update_view = HospitalViewSet.as_view({'patch': 'partial_update'})
    model = Hospital
    model_objects = Hospital.objects
    model_fields = Hospital._meta.fields

    def setUp(self):
        super(HospitalTests, self).setUp()

        # Users
        self.admin: User = new_random_user("is", "admin", is_staff=True)
        self.not_admin: User = new_random_user("not", "admin")


class HospitalCaseRetrieveFilter(CaseRetrieveFilter):
    def __init__(self, name: str, exclude: dict = None):
        self.name = name
        super(HospitalCaseRetrieveFilter, self).__init__(exclude=exclude)


class HospitalGetListTests(HospitalTests):
    def setUp(self):
        super(HospitalGetListTests, self).setUp()
        self.name_pattern = random_str(3).replace(' ', 'e')
        nb_hosps = 500

        self.hosp_names = [
                              random_str(random.randint(5, 15)) for _ in
                              range(nb_hosps - 110)
                          ] + [
                              random_str(random.randint(2, 7))
                              + self.name_pattern
                              + random_str(random.randint(3, 5)) for _ in
                              range(110)
                          ]

        self.list_hosps: List[Hospital] = Hospital.objects.bulk_create([
            Hospital(
                **{
                    'name': name,
                }
            ) for name in self.hosp_names
        ])

    def test_get_all_hosps(self):
        # As a user with no right, I can get all hosps
        case = ListCase(
            to_find=self.list_hosps,
            success=True,
            status=status.HTTP_200_OK,
            user=self.not_admin
        )
        self.check_get_paged_list_case(case)

    def test_get_list_with_params(self):
        # As a user with no right, I can get hosps filtered
        # given query parameters
        basic_case_dict = dict(success=True, status=status.HTTP_200_OK,
                               user=self.not_admin)
        cases = [
            ListCase(
                **basic_case_dict,
                title=f"search={self.name_pattern}",
                to_find=[
                    hosp for hosp in self.list_hosps
                    if self.name_pattern in hosp.name],
                params=dict(search=self.name_pattern)
            )
        ]
        [self.check_get_paged_list_case(case) for case in cases]

    def test_error_not_authenticated(self):
        # As a user not authenticated, I cannot get any hosps
        case = ListCase(
            to_find=[],
            success=False,
            status=status.HTTP_401_UNAUTHORIZED
        )
        self.check_get_paged_list_case(case)


class HospitalCreateTests(HospitalTests):
    def setUp(self):
        super(HospitalCreateTests, self).setUp()

        test_hosp_name = random_str(10)
        self.creation_data = {
            'name': test_hosp_name
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=HospitalCaseRetrieveFilter(
                name=test_hosp_name.strip()
            ),
            user=None, status=None, success=None,
        )

    def test_create_as_admin(self):
        # As a user with is_staff=True, I can create a new hosp
        case = self.basic_create_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_201_CREATED,
        )
        self.check_create_case(case)

    def test_error_create_as_basic_user(self):
        # As a user with is_staff=False, I cannot create a new hosp
        case = self.basic_create_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_create_case(case)


class HospitalPatchTests(HospitalTests):
    def setUp(self):
        super(HospitalPatchTests, self).setUp()

        self.created_data = {
            'name': 'created'
        }
        self.base_data_to_update = {
            'name': 'updated'
        }
        self.basic_patch_case = PatchCase(
            initial_data=self.created_data,
            data_to_update=self.base_data_to_update,
            user=None, status=None, success=None,
        )

    def test_patch_as_admin(self):
        # As a user with is_staff=True, I can edit a hosp
        case = self.basic_patch_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_200_OK,
        )
        self.check_patch_case(case)

    def test_error_patch_as_basic_user(self):
        # As a user with is_staff=False, I cannot edit a hosp
        case = self.basic_patch_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_patch_case(case)


class HospitalDeleteTests(HospitalTests):
    def setUp(self):
        super(HospitalDeleteTests, self).setUp()

        self.created_data = {
            'name': 'created'
        }
        self.basic_delete_case = DeleteCase(
            data_to_delete=self.created_data,
            user=None, status=None, success=None,
        )

    # when we'll be safe with hosp deletion (cascade, deletion, etc.)
    def test_delete_user_as_main_admin(self):
        # As a user with is_staff=True, I can soft-delete a hosp
        case = self.basic_delete_case.clone(
            user=self.admin,
            success=True,
            status=status.HTTP_204_NO_CONTENT,
        )
        self.check_delete_case(case)

    def test_error_delete_user_as_simple_user(self):
        # As a user with is_staff=False, I cannot delete a hosp
        case = self.basic_delete_case.clone(
            user=self.not_admin,
            success=False,
            status=status.HTTP_403_FORBIDDEN,
        )
        self.check_delete_case(case)
