from rest_framework import serializers

from orea.serializers import BaseSerializer
from .models import IntensiveCareService, Bed, Unit, Hospital


class BedSerializer(BaseSerializer):
    current_patient_stay = serializers.PrimaryKeyRelatedField(
        source='current_bed_stay.patient_stay', read_only=True, allow_null=True)

    class Meta:
        model = Bed
        fields = "__all__"


class NestedBedSerializer(BedSerializer):
    bed_description = serializers.CharField(read_only=True)

    def get_fields(self):
        fields = super(BedSerializer, self).get_fields()
        try:
            from patients.serializers import BasicPatientStaySerializer
            fields.update(dict(
                current_patient_stay=BasicPatientStaySerializer(allow_null=True)
            ))
        except ImportError:
            pass
        return fields


class UnitSerializer(BaseSerializer):
    beds = serializers.ListSerializer(
        child=serializers.PrimaryKeyRelatedField(queryset=Bed.objects.all()),
        required=False)

    class Meta:
        model = Unit
        fields = "__all__"


class NestedUnitSerializer(UnitSerializer):
    beds = serializers.ListSerializer(child=NestedBedSerializer())


class IntensiveCareServiceSerializer(BaseSerializer):
    units = serializers.ListSerializer(
        child=serializers.PrimaryKeyRelatedField(read_only=True),
        read_only=True)
    hospital = serializers.PrimaryKeyRelatedField(
        required=True, queryset=Hospital.objects.all())
    hospital_name = serializers.SlugRelatedField(
        slug_field="name", source="hospital", read_only=True)

    class Meta:
        model = IntensiveCareService
        fields = "__all__"


class NestedIntensiveCareServiceSerializer(IntensiveCareServiceSerializer):
    units = serializers.ListSerializer(child=NestedUnitSerializer())
    hospital = serializers.SlugRelatedField(slug_field="name",
                                            queryset=Hospital.objects.all())


class HospitalSerializer(BaseSerializer):
    class Meta:
        model = Hospital
        fields = "__all__"
