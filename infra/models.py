from typing import List

from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from django.db.models.deletion import CASCADE, PROTECT

from orea.models import BaseModel


class Hospital(BaseModel):
    name = models.CharField(max_length=500, unique=True)

    def __str__(self):
        return f"{_('Hospital')} {self.name}"


class IntensiveCareService(BaseModel):
    name = models.CharField(max_length=20, blank=False, null=False)
    hospital = models.ForeignKey(Hospital, on_delete=CASCADE,
                                 blank=False, null=False)

    class Meta:
        unique_together = [["name", "hospital"]]

    def __str__(self):
        return f"{_('Intensive care')} {self.name} - {self.hospital}"


class Unit(BaseModel):
    name = models.CharField(max_length=20, blank=False, null=False)
    intensive_care_service = models.ForeignKey(
        IntensiveCareService, related_name="units", on_delete=PROTECT,
        blank=False, null=False)

    class Meta:
        unique_together = [["name", "intensive_care_service"]]

    def __str__(self):
        return f"{_('unit')} {self.name} ({self.intensive_care_service})"


class Bed(BaseModel):
    unit = models.ForeignKey(Unit, related_name='beds',
                             on_delete=CASCADE, blank=False, null=False)
    unit_index = models.IntegerField(blank=False, null=False)
    usable = models.BooleanField(default=True)

    @property
    def current_bed_stay(self):
        try:
            from patients.models import BedStay
        except ImportError:
            return None

        pref_curr_stays: List[BedStay] = getattr(
            self, 'prefetched_current_stays', None)
        if pref_curr_stays is not None:
            return next(iter(pref_curr_stays), None)
        return self.bed_stays.filter(BedStay.is_current_Q()).first()

    @property
    def current_patient_stay(self):
        return (self.current_bed_stay.patient_stay
                if self.current_bed_stay is not None
                else None)

    def is_busy_between(self, start_date: timezone.datetime,
                        end_date: timezone.datetime):
        return self.bed_stays.all().filter(
            Q(start_date__gte=start_date, start_date__lt=end_date)
            | Q(end_date__gt=start_date, end_date__lte=end_date)
            | (Q(start_date__lt=start_date)
               & (Q(end_date__gte=end_date)
                  | Q(end_date__isnull=True))
               )
        ).first() is not None

    class Meta:
        unique_together = [["unit", "unit_index"]]

    @property
    def str_usable(self):
        return _("usable") if self.usable else _("unusable")

    @property
    def str_busy(self):
        return _("busy") if self.current_bed_stay else _("available")

    def __str__(self):
        return f"{_('Bed')} {self.unit} {self.unit_index} - " \
               f"{self.str_usable} - {self.str_busy}"

    @property
    def bed_description(self):
        return str(self)
