from django.urls import path, include

from orea.urls import OptionalSlashRouter
from . import views

router = OptionalSlashRouter(trailing_slash=False)
router.register(r'beds', views.BedViewSet, basename="beds")
router.register(r'hospitals', views.HospitalViewSet, basename="hospitals")
router.register(r'services', views.IntensiveCareServiceViewSet,
                basename="intensive_care_services")
router.register(r'units', views.UnitViewSet, basename="units")

urlpatterns = [
    path('', include(router.urls)),
]
