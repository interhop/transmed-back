from django.contrib import admin
from .models import IntensiveCareService, Unit, Bed, Hospital


class HospitalAdmin(admin.ModelAdmin):
    pass


class IntensiveCareServiceAdmin(admin.ModelAdmin):
    pass


class UnitAdmin(admin.ModelAdmin):
    pass


class BedAdmin(admin.ModelAdmin):
    pass


admin.site.register(Hospital, HospitalAdmin)
admin.site.register(IntensiveCareService, IntensiveCareServiceAdmin)
admin.site.register(Unit, UnitAdmin)
admin.site.register(Bed, BedAdmin)
