from __future__ import annotations

from datetime import timedelta
from typing import Union

from django.db.models import Model
from django.utils import timezone
from django.utils.datetime_safe import datetime
from rest_framework import status
from rest_framework.test import force_authenticate

from infra.tests.tools import ViewSetTestsWithBasicInfra
from infra.models import Bed
from orea.models import User, BaseModel
from orea.tests_tools import CaseRetrieveFilter, new_random_user, CreateCase, \
    PatchCase, ActionCase
from orea.utils import prettify_json
from patients.models import Patient, PatientStay, BedStay
from patients.views import PatientStayViewSet
from users.models import Access

PATIENTS_URL = "/patients/"


# CASES #######################################################################

class StayCaseRetrieveFilter(CaseRetrieveFilter):
    def __init__(self, patient: Patient, exclude: dict = None):
        self.patient = patient
        super(StayCaseRetrieveFilter, self).__init__(exclude=exclude)


class StayCase:
    def __init__(self, bed: Bed, current_stay: bool = True,
                 stay_start_date: datetime = None):
        self.bed = bed
        self.current_stay = current_stay
        self.stay_start_date = stay_start_date


class StayMoveCase(ActionCase, StayCase):
    def __init__(self, bed: Bed, destination_bed: Bed,
                 current_stay: bool = True, **kwargs):
        super(StayMoveCase, self).__init__(**kwargs)
        StayCase.__init__(self, bed, current_stay)
        self.destination_bed = destination_bed


class StayCloseCase(ActionCase, StayCase):
    def __init__(self, bed: Bed, current_stay: bool = True, **kwargs):
        super(StayCloseCase, self).__init__(**kwargs)
        StayCase.__init__(self, bed, current_stay)


class StayPatchCase(PatchCase, StayCase):
    def __init__(self, stay_start_date: datetime, bed: Bed,
                 current_stay: bool = True, **kwargs):
        super(StayPatchCase, self).__init__(**kwargs)
        StayCase.__init__(self, bed, current_stay, stay_start_date)


# TESTS #######################################################################

class StaysTests(ViewSetTestsWithBasicInfra):
    unupdatable_fields = ["patient"]
    unsettable_default_fields = dict()
    unsettable_fields = []
    alias_fields = {'bed': 'current_bed'}

    objects_url = f"{PATIENTS_URL}/patient-stays"
    retrieve_view = PatientStayViewSet.as_view({'get': 'retrieve'})
    list_view = PatientStayViewSet.as_view({'get': 'list'})
    create_view = PatientStayViewSet.as_view({'post': 'create'})
    delete_view = PatientStayViewSet.as_view({'delete': 'destroy'})
    update_view = PatientStayViewSet.as_view({'patch': 'partial_update'})
    move_view = PatientStayViewSet.as_view({'patch': 'move'})
    close_view = PatientStayViewSet.as_view({'patch': 'close'})
    model = PatientStay
    model_objects = PatientStay.objects
    model_fields = PatientStay._meta.fields

    def init_case_obj(self, case: Union[ActionCase, StayCase]):
        obj_id = self.model_objects.create(**case.initial_data).pk
        obj = self.model_objects.get(pk=obj_id)

        start_date = case.stay_start_date or (timezone.now() - timedelta(days=2))

        BedStay.objects.create(
            start_date=start_date,
            end_date=((start_date + timedelta(days=1))
                      if not case.current_stay else None),
            bed=case.bed,
            patient_stay=obj,
        )
        return obj

    def check_is_created(self, result: PatientStay,
                         request_model: dict = None, **kwargs):
        # needed particular case because start_date time will be set to 00:00
        request_model = request_model or dict()
        self.assertIsNotNone(result)
        self.assertTrue(isinstance(result, PatientStay))

        [
            self.assertEqual(
                getattr(result, f), v,
                f"Error, model's {f} should have been set to a default."
            ) for [f, v] in self.unsettable_default_fields.items()
        ]

        for f in request_model:
            if f not in self.unsettable_fields:
                if (
                        f == 'start_date' and
                        datetime.fromisoformat(request_model.get(f)).date()
                        == timezone.now().date()
                ):
                    delta = (result.start_date - timezone.now())
                    self.assertAlmostEqual(
                        delta.total_seconds(), 0, delta=1,
                        msg=f"Error, model's {f} should have been set "
                            f"with time at now."
                    )
                elif f == 'bed':
                    self.assertEqual(
                        result.current_bed.pk, request_model.get(f),
                        f"Error, model's {f} should have been set."
                    )
                elif isinstance(getattr(result, f), BaseModel):
                    self.assertEqual(
                        getattr(result, f).pk, request_model.get(f),
                        f"Error, model's {f} should have been set."
                    )
                else:
                    self.assertEqual(
                        getattr(result, f), request_model.get(f),
                        f"Error, model's {f} should have been set."
                    )
            else:
                self.assertNotEqual(
                    getattr(result, f), request_model.get(f),
                    f"Error, model's {f} should not have been set."
                )

    def setUp(self):
        super(StaysTests, self).setUp()
        self.admin_user: User = new_random_user("admin", "main", is_staff=True)
        self.user_1: User = new_random_user("user", "1")
        self.access_user_1_serv_1: Access = Access.objects.create(
            intensive_care_service=self.serv_1,
            user=self.user_1,
            start_datetime=timezone.now() - timedelta(days=1),
            end_datetime=timezone.now() + timedelta(days=1),
            status=Access.Status.validated,
            right_edit=False,
            right_review=True
        )


class CreateStaysTests(StaysTests):
    def setUp(self):
        super(CreateStaysTests, self).setUp()
        self.patient: Patient = Patient.objects.create(
            local_id='patient_1',
        )
        self.test_bed = self.hosp_1_beds[0]
        self.creation_data = {
            'bed': self.test_bed.pk,
            'patient': self.patient.pk,
            'start_date': timezone.now().date().isoformat()
        }
        self.basic_create_case = CreateCase(
            data=self.creation_data,
            retrieve_filter=StayCaseRetrieveFilter(patient=self.patient),
            user=self.user_1, status=status.HTTP_201_CREATED, success=True,
        )

    def test_create_stay(self):
        # As a user with access to a service,
        # I can create a stay in this service
        self.check_create_case(self.basic_create_case)

    def test_err_create_stay_wrong_data(self):
        # As a user, I cannot create a stay with wrong data
        cases = [self.basic_create_case.clone(
            data={**self.creation_data, k: v},
            user=self.user_1,
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        ) for k, v in [
            ('start_date', (timezone.now() + timedelta(days=1)).isoformat())
        ]]
        [self.check_create_case(case) for case in cases]

    def test_err_create_stay_bed_busy(self):
        # Even as an admin user, I cannot create a stay to a bed occupied
        other_patient: Patient = Patient.objects.create(
            local_id='o_patient',
        )
        stay: PatientStay = PatientStay.objects.create(
            patient=other_patient
        )
        BedStay.objects.create(
            start_date=timezone.now().date(),
            bed=self.test_bed,
            patient_stay=stay
        )

        case = self.basic_create_case.clone(
            user=self.admin_user,
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        )
        self.check_create_case(case)

    def test_err_create_stay_bed_previously_busy(self):
        # Even as an admin user, I cannot create a stay to a bed that was at
        # some point occupied since the start date I provide
        other_patient: Patient = Patient.objects.create(
            local_id='o_patient',
        )
        stay: PatientStay = PatientStay.objects.create(
            patient=other_patient
        )
        BedStay.objects.create(
            start_date=timezone.now().date() - timedelta(days=2),
            end_date=timezone.now().date() - timedelta(days=1),
            bed=self.test_bed,
            patient_stay=stay
        )

        case = self.basic_create_case.clone(
            data={**self.creation_data,
                  'start_date': timezone.now() - timedelta(days=3)},
            user=self.admin_user,
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        )
        self.check_create_case(case)

    def test_err_create_stay_unusable_bed(self):
        # Even as an admin user, I cannot create a stay to a bed unusable
        self.test_bed.usable = False
        self.test_bed.save()

        case = self.basic_create_case.clone(
            user=self.admin_user,
            status=status.HTTP_400_BAD_REQUEST,
            success=False,
        )
        self.check_create_case(case)


class PatchStayTests(StaysTests):
    def check_is_updated(self, result: BaseModel, patch_case: PatchCase,
                         origin: BaseModel):
        return super().check_is_updated(result, patch_case, origin)

    def check_patch_case(self, case: StayPatchCase, obj: Model = None):
        obj: PatientStay = self.init_case_obj(case)
        return super().check_patch_case(case, obj)

    def setUp(self):
        super().setUp()
        self.test_bed = self.hosp_1_beds[0]
        self.test_start_date = timezone.now() - timedelta(days=1)
        self.patch_start_date = self.test_start_date - timedelta(days=2)
        patient: Patient = Patient.objects.create(local_id='test')
        patient_2: Patient = Patient.objects.create(local_id='test_2')

        self.basic_patch_case = StayPatchCase(
            initial_data=dict(
                patient=patient,
                created_by=self.user_1,
                hospitalisation_cause="test",
                severity=PatientStay.StaySeverity.stable
            ),
            bed=self.test_bed,
            stay_start_date=self.test_start_date,
            current_stay=True,
            data_to_update=dict(
                patient=patient_2.pk,
                hospitalisation_cause="updated",
                severity=PatientStay.StaySeverity.unstable,
                start_date=self.patch_start_date
            ),
            user=self.user_1,
            success=True,
            status=status.HTTP_200_OK,
        )

    def test_patch_stay(self):
        # As a user with access to a service, I can patch a stay in this service
        self.check_patch_case(self.basic_patch_case)

    def test_err_patch_stay_bed(self):
        # Even as admin user, I cannot patch the bed of a stay
        # (must use /move request)
        self.check_patch_case(self.basic_patch_case.clone(
            data_to_update=dict(bed=self.hosp_1_beds[1].pk),
            user=self.admin_user,
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
        ))

    def test_err_patch_stay_future_start_date(self):
        # Even as admin user, I cannot patch the start_date of a stay
        # in the future
        self.check_patch_case(self.basic_patch_case.clone(
            data_to_update=dict(start_date=timezone.now() + timedelta(days=1)),
            user=self.admin_user,
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
        ))

    def test_err_patch_stay_start_date_forward(self):
        # Even as admin user, I cannot patch the start_date of a stay
        # in the future
        self.check_patch_case(self.basic_patch_case.clone(
            data_to_update=dict(
                start_date=self.test_start_date + timedelta(hours=1)
            ),
            user=self.admin_user,
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
        ))

    def test_err_patch_stay_start_date_on_busy_bed(self):
        # Even as admin user, I cannot patch the start_date of a stay
        # on a date where the first bed of the stay was busy
        o_patient: Patient = Patient.objects.create(local_id='o_patient')
        o_patient_stay: PatientStay = PatientStay.objects.create(
            patient=o_patient, created_by=self.user_1
        )
        BedStay.objects.create(
            bed=self.test_bed, patient_stay=o_patient_stay,
            start_date=self.patch_start_date - timedelta(days=1),
            end_date=self.patch_start_date + timedelta(days=1)
        )
        self.check_patch_case(self.basic_patch_case.clone(
            user=self.admin_user,
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
        ))

    def test_err_patch_stay_bed_not_managed(self):
        # As a user, I cannot patch a stay in a service I don't have access to
        case = self.basic_patch_case.clone(
            bed=self.hosp_2_beds[0],
            success=False,
            status=status.HTTP_404_NOT_FOUND
        )
        self.check_patch_case(case)

    def test_patch_stay_not_current(self):
        # As a user with access to a service, I can patch a stay that
        # was in this service even if it is closed
        case = self.basic_patch_case.clone(
            current_stay=False,
            success=True,
            status=status.HTTP_200_OK
        )
        self.check_patch_case(case)


class MoveStayTests(StaysTests):
    def check_move_case(self, case: StayMoveCase):
        obj: PatientStay = self.init_case_obj(case)
        origin_bed = obj.current_bed

        request = self.factory.patch(
            self.objects_url, dict(new_bed=case.destination_bed.pk),
            format='json'
        )
        force_authenticate(request, case.user)
        response = self.__class__.move_view(
            request, pk=obj.pk
        )
        response.render()

        self.assertEqual(
            response.status_code, case.status,
            msg=(f"{case.description}"
                 + (f" -> {prettify_json(response.content)}"
                    if response.content else "")),
        )

        new_obj: PatientStay = self.model_objects.filter(pk=obj.pk).first()

        if case.success:
            self.assertEqual(new_obj.current_bed.pk, case.destination_bed.pk)
        else:
            (self.assertEqual(new_obj.current_bed.pk, origin_bed.pk)
             if origin_bed is not None
             else self.assertIsNone(new_obj.current_bed))

    def setUp(self):
        super().setUp()

        patient: Patient = Patient.objects.create(local_id='test')
        self.user_2: User = new_random_user("user", "2")

        self.patient_2: Patient = Patient.objects.create(local_id='test_2')
        patient_2_stay: PatientStay = PatientStay.objects.create(
            patient=self.patient_2, created_by=self.user_2
        )
        BedStay.objects.create(
            bed=self.hosp_1_beds[1], patient_stay=patient_2_stay,
            start_date=timezone.now() - timedelta(days=1)
        )

        self.basic_move_case = StayMoveCase(
            initial_data=dict(
                patient=patient,
                created_by=self.user_2,
                hospitalisation_cause="",
                severity=PatientStay.StaySeverity.stable
            ),
            bed=self.hosp_1_beds[0],
            current_stay=True,
            destination_bed=self.hosp_1_beds[1],
            user=self.user_1,
            success=True,
            status=status.HTTP_200_OK,
        )

    def test_move_stay(self):
        # As a user with access to a service,
        # I can move a stay in this service
        self.check_move_case(self.basic_move_case)

    def test_move_stay_occupied_bed_will_swap(self):
        # As a user with access to a service,
        # I can move a stay in this service to an occupied bed,
        # it wil swap the beds from the two stays
        case = self.basic_move_case.clone(
            destination_bed=self.patient_2.current_stay.current_bed,
        )
        self.check_move_case(case)
        self.assertEqual(self.patient_2.current_stay.current_bed.pk,
                         case.bed.pk)

    def test_err_move_stay_bed_destination_not_managed(self):
        # As a user with access to a service,
        # I cannot move a stay to a bed in a service I don't have access to
        case = self.basic_move_case.clone(
            destination_bed=self.hosp_2_beds[0],
            success=False,
            status=status.HTTP_404_NOT_FOUND
        )
        self.check_move_case(case)

    def test_err_move_stay_same_bed(self):
        # As a user with access to a service,
        # I cannot move a stay to its same bed
        case = self.basic_move_case.clone(
            destination_bed=self.hosp_1_beds[0],
            success=False,
            status=status.HTTP_400_BAD_REQUEST
        )
        self.check_move_case(case)

    def test_err_move_stay_bed_origin_not_managed(self):
        # As a user,
        # I cannot move a stay from a service I don't have access to
        case = self.basic_move_case.clone(
            bed=self.hosp_2_beds[0],
            success=False,
            status=status.HTTP_404_NOT_FOUND
        )
        self.check_move_case(case)

    def test_err_move_stay_not_current_as_admin(self):
        # Even as an admin user,
        # I cannot move a stay that is closed
        case = self.basic_move_case.clone(
            user=self.admin_user,
            current_stay=False,
            success=False,
            status=status.HTTP_400_BAD_REQUEST
        )
        self.check_move_case(case)

    def test_err_move_stay_not_current(self):
        # As a user,
        # I cannot move a stay that is closed
        case = self.basic_move_case.clone(
            current_stay=False,
            success=False,
            status=status.HTTP_400_BAD_REQUEST
        )
        self.check_move_case(case)


class CloseStayTests(StaysTests):
    def check_close_case(self, case: StayCloseCase):
        obj: PatientStay = self.init_case_obj(case)

        request = self.factory.patch(
            self.objects_url, format='json'
        )
        force_authenticate(request, case.user)
        response = self.__class__.close_view(
            request, pk=obj.pk
        )
        response.render()

        self.assertEqual(
            response.status_code, case.status,
            msg=(f"{case.description}"
                 + (f" -> {prettify_json(response.content)}"
                    if response.content else "")),
        )

        new_obj: PatientStay = self.model_objects.filter(pk=obj.pk).first()
        new_obj_last_bed_stay: BedStay = (new_obj.bed_stays
                                          .order_by('-start_date').first())

        if case.success:
            delta = (new_obj_last_bed_stay.end_date - timezone.now())
            self.assertAlmostEqual(delta.total_seconds(), 0, delta=1)
            self.assertIsNone(new_obj.current_bed_stay)
            self.assertIsNone(new_obj.current_bed)
        else:
            if case.current_stay:
                self.assertIsNotNone(new_obj.current_bed)

    def setUp(self):
        super().setUp()

        patient: Patient = Patient.objects.create(local_id='test')
        self.user_2: User = new_random_user("user", "2")

        self.basic_close_case = StayCloseCase(
            initial_data=dict(
                patient=patient,
                created_by=self.user_2,
                hospitalisation_cause="",
                severity=PatientStay.StaySeverity.stable
            ),
            bed=self.hosp_1_beds[0],
            current_stay=True,
            user=self.user_1,
            success=True,
            status=status.HTTP_200_OK,
        )

    def test_close_stay(self):
        # As a user with access to a service,
        # I can close a stay from this service
        self.check_close_case(self.basic_close_case)

    def test_err_close_stay_bed_not_managed(self):
        # As a user,
        # I cannot close a stay from a service I don't have access to
        case = self.basic_close_case.clone(
            bed=self.hosp_2_beds[0],
            success=False,
            status=status.HTTP_404_NOT_FOUND
        )
        self.check_close_case(case)

    def test_err_close_stay_not_current(self):
        # As a user,
        # I cannot close a stay that is closed
        case = self.basic_close_case.clone(
            current_stay=False,
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
        )
        self.check_close_case(case)

    def test_err_close_stay_not_current_as_admin(self):
        # Even as an admin user,
        # I cannot close a stay that is closed
        case = self.basic_close_case.clone(
            current_stay=False,
            user=self.admin_user,
            success=False,
            status=status.HTTP_400_BAD_REQUEST,
        )
        self.check_close_case(case)
