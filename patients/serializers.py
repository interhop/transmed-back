from datetime import datetime

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from infra.models import Bed
from orea.models import User
from orea.serializers import BaseSerializer
from orea.utils import NoFutureDateTimeField
from .models import Patient, PatientStay, BedStay, PatientNote, \
    TreatmentLimitation, DetectionMeasure, FailureMeasure


class PatientPKRelatedField(serializers.RelatedField):
    def to_internal_value(self, data):
        return serializers.PrimaryKeyRelatedField(
            queryset=self.get_queryset()
        ).to_internal_value(data)

    def to_representation(self, value):
        return PatientSerializer().to_representation(value)


class PatientStayPKField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context.get("request", None).user
        if user is None:
            raise Exception(
                "Internal error: No context request provided to serializer")
        q = super(PatientStayPKField, self).get_queryset()
        if user.is_staff:
            return q
        return q.filter(
            bed_stays__bed__unit__intensive_care_service__in=(
                user.authorized_intensive_care_services)
        ).distinct()


class PKCreatedBy(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context.get("request", None).user
        if user is None:
            raise Exception(
                "Internal error: No context request provided to serializer")
        return super(PKCreatedBy, self).get_queryset().filter(pk=user.pk)


class PatientNoteSerializer(BaseSerializer):
    created_by = PKCreatedBy(queryset=User.objects.all(),
                             default=serializers.CurrentUserDefault())
    stay = PatientStayPKField(queryset=PatientStay.objects.all(),
                              allow_null=False)
    type = serializers.CharField(required=True)

    class Meta:
        model = PatientNote
        fields = "__all__"

    def create(self, validated_data):
        type_ = validated_data.get('type')
        stay: PatientStay = validated_data.get('stay')

        if stay.is_finished:
            raise ValidationError(_(
                "Cannot add a note to a stay that is closed."
            ))

        if type_ != PatientNote.NoteType.general:
            existing = PatientNote.objects.filter(
                type=type_, stay=stay).first()
            if existing is not None:
                raise ValidationError(_(
                    f"Note of type {type_} for this stay "
                    f"already exists. Please use "
                    f"PATCH /patients/notes/{existing.pk}"
                ))
        return super(PatientNoteSerializer, self).create(validated_data)


class PatchPatientNoteSerializer(PatientNoteSerializer):
    created_by = PKCreatedBy(read_only=True)
    stay = PatientStayPKField(read_only=True)
    type = serializers.CharField(read_only=True)


class MeasureSerializer(BaseSerializer):
    created_by = PKCreatedBy(queryset=User.objects.all(),
                             default=serializers.CurrentUserDefault())
    stay = PatientStayPKField(queryset=PatientStay.objects.all(),
                              allow_null=False)
    measure_datetime = NoFutureDateTimeField()

    def create(self, validated_data):
        stay: PatientStay = validated_data.get('stay')
        if stay.current_bed_stay is None:
            raise ValidationError(_('Selected stay is already finished'))

        return super().create(validated_data)


class TreatmentLimitationSerializer(MeasureSerializer):
    class Meta:
        model = TreatmentLimitation
        fields = "__all__"


class DetectionSerializer(MeasureSerializer):
    class Meta:
        model = DetectionMeasure
        fields = "__all__"


class FailureMeasureSerializer(MeasureSerializer):
    class Meta:
        model = FailureMeasure
        fields = "__all__"


class PatchFailureMeasureSerializer(FailureMeasureSerializer):
    created_by = PKCreatedBy(read_only=True)
    failure_type = serializers.CharField(read_only=True)


class BedPKField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context.get("request", None).user
        if user is None:
            raise Exception(
                "Internal error: No context request provided to serializer")
        q = super(BedPKField, self).get_queryset()
        if user.is_staff:
            return q
        return q.filter(
            unit__intensive_care_service__in=(
                user.authorized_intensive_care_services)
        ).distinct()


class PatientStaySerializer(BaseSerializer):
    created_by = PKCreatedBy(queryset=User.objects.all(),
                             default=serializers.CurrentUserDefault())
    patient = PatientPKRelatedField(queryset=Patient.objects.all())
    bed = BedPKField(queryset=Bed.objects.all(), required=True,
                     source='current_bed')

    # properties
    start_date = NoFutureDateTimeField(default=timezone.now)
    bed_description = serializers.CharField(read_only=True)
    end_date = serializers.DateTimeField(read_only=True)
    is_finished = serializers.BooleanField(read_only=True)

    notes = serializers.ListSerializer(
        child=PatientNoteSerializer(), read_only=True)
    treatment_limitations = serializers.ListSerializer(
        child=TreatmentLimitationSerializer(), read_only=True,
        source='treatmentlimitation_set')
    detections = serializers.ListSerializer(
        child=DetectionSerializer(), read_only=True,
        source='detectionmeasure_set')
    failure_measures = serializers.ListSerializer(
        child=FailureMeasureSerializer(), read_only=True,
        source='failuremeasure_set')
    todo_list = PatientNoteSerializer(allow_null=True, required=False)

    class Meta:
        model = PatientStay
        fields = "__all__"

    def validate_bed(self, value: Bed):
        if self.instance and value != self.instance.current_bed:
            raise serializers.ValidationError(
                f"{_('To update the bed, use')} "
                f"/patients/unit-stays/{self.instance.pk}/move"
            )
        if not value.usable:
            raise serializers.ValidationError(_("Bed requested is not usable"))
        return value

    def validate_start_date(self, value: timezone.datetime):
        instance: PatientStay = self.instance

        if instance and value != instance.start_date:
            if value > instance.start_date:
                raise serializers.ValidationError(_(
                    "Cannot move start_date forward"
                ))

            first_bed_stay = instance.first_bed_stay
            first_bed = first_bed_stay.bed

            if first_bed.is_busy_between(value, instance.start_date):
                msg = _("First bed used by this patient was busy between the "
                        "new and the current start dates. First bed was")
                raise serializers.ValidationError(f"{msg} {first_bed}")
        return value

    def create(self, validated_data):
        bed = validated_data.pop('current_bed', validated_data.get('bed', None))
        start_date = validated_data.pop('start_date')
        if isinstance(start_date, datetime):
            if start_date.date() == timezone.now().date():
                start_date = timezone.now()
        if bed.is_busy_between(start_date, timezone.now()):
            raise ValidationError(_("Bed requested was busy since the"
                                    "start_date you requested"))

        instance = super(PatientStaySerializer, self).create(validated_data)

        if bed is not None:
            BedStay.objects.create(
                start_date=start_date,
                bed=bed,
                patient_stay=instance
            )
        return instance

    def update(self, instance: PatientStay, validated_data):
        validated_data.pop('patient', None)
        start_date = validated_data.pop('start_date', None)
        if isinstance(start_date, datetime):
            first_bed_stay = instance.first_bed_stay
            first_bed_stay.start_date = start_date
            first_bed_stay.save()
        return super().update(instance, validated_data)


class PatientSerializer(BaseSerializer):
    current_stay = serializers.UUIDField(
        read_only=True, source="current_stay.pk")
    allergies = serializers.JSONField()
    antecedents = serializers.JSONField()
    size_cm = serializers.IntegerField(min_value=0)
    weight_kg = serializers.IntegerField(min_value=0)

    class Meta:
        model = Patient
        fields = "__all__"


class ReadPatientStaySerializer(PatientStaySerializer):
    patient = PatientSerializer()


class BasicPatientStaySerializer(BaseSerializer):
    created_by = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    patient = PatientSerializer()
    failure_measures = serializers.ListSerializer(
        child=FailureMeasureSerializer(), read_only=True,
        source='failuremeasure_set')
    detections = serializers.ListSerializer(
        child=DetectionSerializer(), read_only=True,
        source='detectionmeasure_set')
    todo_list = PatientNoteSerializer(allow_null=True)
    start_date = serializers.DateTimeField(default=timezone.now)
    end_date = serializers.DateTimeField(allow_null=True, read_only=True)

    class Meta:
        model = PatientStay
        fields = "__all__"
