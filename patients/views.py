from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from infra.models import Bed
from infra.views import BedViewSet
from orea.models import User
from orea.views import BaseViewSet, BaseFilterSet
from .models import Patient, PatientStay, TreatmentLimitation, \
    DetectionMeasure, FailureMeasure, PatientNote, Measure
from .permissions import DeletableByAdmin
from .serializers import PatientSerializer, PatientStaySerializer, \
    TreatmentLimitationSerializer, DetectionSerializer, \
    FailureMeasureSerializer, PatientNoteSerializer, \
    ReadPatientStaySerializer, PatchFailureMeasureSerializer, \
    PatchPatientNoteSerializer


class PatientFilterSet(BaseFilterSet):
    class Meta:
        model = Patient
        fields = ("first_name", "last_name", "birth_date",
                  "sex", "size_cm", "weight_kg")


class PatientViewSet(BaseViewSet):
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer
    permission_classes = (DeletableByAdmin,)

    search_fields = ("local_id", "first_name", "last_name",
                     "antecedents", "allergies",)
    filterset_class = PatientFilterSet

    def get_queryset(self):
        q = super(PatientViewSet, self).get_queryset()

        user: User = self.request.user
        if user.is_staff:
            return q
        return q.filter(
            stays__bed_stays__bed__unit__intensive_care_service__in=(
                user.authorized_intensive_care_services)).distinct()

    def create(self, request, *args, **kwargs):
        existing_patient: Patient = Patient.objects.filter(
            local_id=request.data.get("local_id")).first()
        if existing_patient:
            if existing_patient.current_stay:
                headers = self.default_response_headers
                return Response(dict(
                    msg=f"Ce patient est déjà en réanimation : "
                        f"\n{existing_patient.current_stay}"
                ), status=status.HTTP_409_CONFLICT, headers=headers)

            serializer = self.get_serializer(existing_patient,
                                             data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return super(PatientViewSet, self).create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        patient: Patient = self.get_object()
        if patient.current_stay:
            return Response(
                data=dict(msg=_('Patient is currently in a service')),
                status=status.HTTP_400_BAD_REQUEST
            )
        return super(PatientViewSet, self).destroy(request, *args, **kwargs)


class PatientStayFilterSet(BaseFilterSet):
    class Meta:
        model = PatientStay
        fields = ("uuid",)


class PatientStayViewSet(BaseViewSet):
    queryset = PatientStay.objects.all()
    serializer_class = PatientStaySerializer
    read_serializer_class = ReadPatientStaySerializer
    permission_classes = (IsAuthenticated,)
    filterset_class = PatientStayFilterSet

    def get_queryset(self):
        q = super(PatientStayViewSet, self).get_queryset()
        user: User = self.request.user
        if user.is_staff:
            return q

        authorized_reas = user.authorized_intensive_care_services.all()
        return q.filter(
            bed_stays__bed__unit__intensive_care_service__in=authorized_reas
        ).distinct().prefetch_related("bed_stays")

    @action(detail=True, methods=['patch'],
            permission_classes=(IsAuthenticated,), url_path="move")
    def move(self, *args, **kwargs):
        stay: PatientStay = self.get_object()
        new_bed_id = self.request.data.get('new_bed', None)
        new_bed: Bed = (BedViewSet(request=self.request).get_queryset()
                        .filter(pk=new_bed_id).first())

        if new_bed is None:
            return Response(data=dict(msg=_('New bed not found')),
                            status=status.HTTP_404_NOT_FOUND)

        try:
            stay.move_bed(new_bed)
        except Exception as e:
            return Response(data=dict(
                msg=str(e),
            ), status=status.HTTP_400_BAD_REQUEST)

        return Response(self.get_serializer_class()(stay).data,
                        status=status.HTTP_200_OK)

    @action(detail=True, methods=['patch'],
            permission_classes=(IsAuthenticated,), url_path="close")
    def close(self, *args, **kwargs):
        stay: PatientStay = self.get_object()
        try:
            stay.close()
        except Exception as e:
            return Response(data=dict(
                msg=str(e),
            ), status=status.HTTP_400_BAD_REQUEST)
        return Response(self.get_serializer_class()(stay).data,
                        status=status.HTTP_200_OK)


class MeasureViewSet(BaseViewSet):
    permission_classes = (IsAuthenticated,)
    http_method_names = ("get", "post", "delete")

    def get_queryset(self):
        q = super(MeasureViewSet, self).get_queryset()
        user: User = self.request.user
        if user.is_staff:
            return q

        authorized_reas = user.authorized_intensive_care_services.all()
        return q.filter(
            stay__bed_stays__bed__unit__intensive_care_service__in=(
                authorized_reas)
        ).distinct()

    def update(self, request, *args, **kwargs):
        meas: Measure = self.get_object()
        if meas.stay.is_finished:
            raise ValidationError(_(
                'Cannot update some data from a finished stay'))
        return super().update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        meas: Measure = self.get_object()
        if meas.stay.is_finished:
            raise ValidationError(_(
                'Cannot update some data from a finished stay'))
        return super().destroy(request, *args, **kwargs)


class TreatmentLimitationFilterSet(BaseFilterSet):
    class Meta:
        model = TreatmentLimitation
        fields = "__all__"


class TreatmentLimitationViewSet(MeasureViewSet):
    queryset = TreatmentLimitation.objects.all()
    serializer_class = TreatmentLimitationSerializer
    search_fields = ("other_stops", "treatment_limitations_comments")
    filterset_class = TreatmentLimitationFilterSet


class DetectionMeasureFilterSet(BaseFilterSet):
    class Meta:
        model = DetectionMeasure
        fields = "__all__"


class DetectionMeasureViewSet(MeasureViewSet):
    queryset = DetectionMeasure.objects.all()
    serializer_class = DetectionSerializer
    filterset_class = DetectionMeasureFilterSet


class FailureMeasureFilterSet(BaseFilterSet):
    class Meta:
        model = FailureMeasure
        fields = "__all__"


class FailureMeasureViewSet(MeasureViewSet):
    queryset = FailureMeasure.objects.all()
    serializer_class = FailureMeasureSerializer
    filterset_class = FailureMeasureFilterSet
    http_method_names = ("patch", "get", "post", "delete")

    def get_serializer_class(self):
        if self.request.method == "PATCH":
            return PatchFailureMeasureSerializer
        else:
            return super().get_serializer_class()


class PatientNoteFilterSet(BaseFilterSet):
    class Meta:
        model = PatientNote
        fields = "__all__"


class PatientNoteViewSet(MeasureViewSet):
    queryset = PatientNote.objects.all()
    serializer_class = PatientNoteSerializer
    filterset_class = PatientNoteFilterSet
    http_method_names = ("get", "post", "patch", "delete")

    def get_serializer_class(self):
        if self.request.method == "PATCH":
            return PatchPatientNoteSerializer
        else:
            return super().get_serializer_class()
