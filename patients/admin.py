from django.contrib import admin
# from .models import Patient, PatientStay, BedStay, PatientNote,\
#     FailureMeasure, DetectionMeasure, TreatmentLimitation


class PatientAdmin(admin.ModelAdmin):
    pass


class PatientStayAdmin(admin.ModelAdmin):
    pass


class BedStayAdmin(admin.ModelAdmin):
    pass


class PatientNoteAdmin(admin.ModelAdmin):
    pass


class FailureMeasureAdmin(admin.ModelAdmin):
    pass


class DetectionMeasureAdmin(admin.ModelAdmin):
    pass


class TreatmentLimitationAdmin(admin.ModelAdmin):
    pass


# admin.site.register(Patient, PatientAdmin)
# admin.site.register(PatientStay, PatientStayAdmin)
# admin.site.register(BedStay, BedStayAdmin)
# admin.site.register(PatientNote, PatientNoteAdmin)
# admin.site.register(FailureMeasure, FailureMeasureAdmin)
# admin.site.register(DetectionMeasure, DetectionMeasureAdmin)
# admin.site.register(TreatmentLimitation, TreatmentLimitationAdmin)
