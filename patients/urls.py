from django.urls import path, include

from orea.urls import OptionalSlashRouter
from .views import PatientViewSet, PatientStayViewSet, FailureMeasureViewSet, \
    TreatmentLimitationViewSet, DetectionMeasureViewSet, PatientNoteViewSet

router = OptionalSlashRouter()

router.register(r'detection-measures', DetectionMeasureViewSet,
                basename="detection_measures")
router.register(r'failure-measures', FailureMeasureViewSet,
                basename="failure_measures")
router.register(r'patients', PatientViewSet, basename="patients")
router.register(r'patient-notes', PatientNoteViewSet, basename="patient_notes")
router.register(r'patient-stays', PatientStayViewSet, basename="patient_stays")
router.register(r'treatment-limitations', TreatmentLimitationViewSet,
                basename="treatment_limitations")

urlpatterns = [
    path('', include(router.urls)),
]
