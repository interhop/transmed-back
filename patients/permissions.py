from rest_framework import permissions

from orea.models import BaseModel, User


class DeletableByAdmin(permissions.IsAuthenticated):
    def has_object_permission(self, request, view, obj: BaseModel):
        if request.method in ["DELETE"]:
            user: User = request.user
            return user.is_staff
        return True
