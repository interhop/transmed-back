from typing import Dict, List

from rest_framework import serializers
from rest_framework.fields import Field, _UnvalidatedField


class MyListField(Field):
    child = _UnvalidatedField()
    initial = []
    default_error_messages = {}

    def to_representation(self, data):
        """
        List of object instances -> List of dicts of primitive datatypes.
        """
        return data.split(',')


def get_description(issue: Dict) -> str:
    content = issue.get('description', '')
    if '\n/votes' not in content:
        return content
    return content.split('\n/votes')[0]


def get_votes(issue: Dict) -> List[str]:
    content = issue.get('description', '')
    if '/votes' not in content:
        return 0

    votes_part = content.split('/votes')[-1]
    return [id.strip() for id in votes_part.split('\n-') if len(id.strip())]


def get_sp(issue: Dict) -> int:
    time_stats: Dict = issue.get('time_stats', {})
    time_estimate = time_stats.get('time_estimate', 0)
    return time_estimate / 60


class ApiIssueSerializer(serializers.Serializer):
    title = serializers.CharField(allow_null=False)
    id = serializers.IntegerField(allow_null=False)
    iid = serializers.IntegerField(allow_null=False)
    project_id = serializers.IntegerField(allow_null=False)
    description = serializers.CharField()

    state = serializers.CharField()
    created_at = serializers.DateTimeField(allow_null=False)
    updated_at = serializers.DateTimeField(allow_null=False)
    closed_at = serializers.DateTimeField(allow_null=True)
    closed_by = serializers.DateTimeField(allow_null=True)

    labels = serializers.ListField(child=serializers.CharField())
    type = serializers.CharField(allow_null=True)
    issue_type = serializers.CharField(allow_null=True)
    web_url = serializers.CharField(allow_null=True)

    votes = serializers.ListSerializer(
        child=serializers.IntegerField(allow_null=True),
        allow_null=True, read_only=True)
    sp = serializers.IntegerField(allow_null=True, read_only=True)

    def to_representation(self, instance):
        return {
            **super(ApiIssueSerializer, self).to_representation(instance),
            'original_description': instance.get('description'),
            'description': get_description(instance),
            'votes': get_votes(instance),
            'sp': get_sp(instance),
        }


class IssuePostSerializer(serializers.Serializer):
    title = serializers.CharField(required=True)
    description = serializers.CharField(required=True)
    label = serializers.CharField(required=True)
    attachment = serializers.FileField(required=False)
