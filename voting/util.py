from collections.abc import Iterable
from typing import Dict

import requests
from django.http import Http404
from requests import Response
from rest_framework import status

from orea.settings import VOTING_API_URL, VOTING_PROJECT_ID, \
    VOTING_PRIVATE_TOKEN, VOTING_GROUP_ID


def req_group(method, end, data: Dict = {}, params: Dict = {}) -> Response:
    url = (f"{VOTING_API_URL}/groups/"
           f"{VOTING_GROUP_ID}/{end}")

    return getattr(requests, method)(
        url,
        headers={"PRIVATE-TOKEN": VOTING_PRIVATE_TOKEN},
        data=data, params=params)


def req_project(
        method, end, project_id: int = VOTING_PROJECT_ID,
        data: Dict = {}, params: Dict = {}
) -> Response:
    url = (f"{VOTING_API_URL}/projects/"
           f"{project_id}/{end}")

    return getattr(requests, method)(
        url,
        headers={"PRIVATE-TOKEN": VOTING_PRIVATE_TOKEN},
        data=data, params=params)


class AttachmentReturned:
    def __init__(self, **kwargs):
        resp = kwargs.get('resp', None)
        if resp is not None and isinstance(resp, requests.Response):
            res = resp.json()
            self.alt: str = res.get('alt', None)
            self.url: str = res.get('url', None)
            self.full_path: str = res.get('full_path', None)
            self.markdown: str = res.get('markdown', None)
        else:
            self.alt: str = kwargs.get('alt', None)
            self.url: str = kwargs.get('url', None)
            self.full_path: str = kwargs.get('full_path', None)
            self.markdown: str = kwargs.get('markdown', None)


def post_gitlab_attachment(file) -> requests.Response:
    url = f"{VOTING_API_URL}/projects/" \
          f"{VOTING_PROJECT_ID}/uploads"
    return requests.post(
        url,
        headers={
            "PRIVATE-TOKEN": VOTING_PRIVATE_TOKEN,
        },
        files=dict(file=file)
    )


def post_gitlab_issue(data: dict = {}):
    url = f"{VOTING_API_URL}/projects/" \
          f"{VOTING_PROJECT_ID}/issues"

    return requests.post(
        url,
        headers={"PRIVATE-TOKEN": VOTING_PRIVATE_TOKEN},
        data=data
    )


def get_issue(project_id: int, id: int) -> Dict:
    resp = req_project("get", f"issues/{id}", project_id=project_id)
    try:
        data = resp.json()
    except Exception as e:
        raise Exception(f"Could not fetch issue: {e}")

    return data


def get_issues(params: Dict):
    resp = req_group("get", "issues", params=params)
    try:
        data = resp.json()
    except Exception as e:
        raise Exception(f"Could not fetch issues: {e}")

    return data


def patch_issue(issue: Dict):
    if 'project_id' not in issue or 'iid' not in issue:
        raise Exception(f"Internal error while patching issue, "
                        f"missing 'project_id' or 'iid': {issue}")

    issue['labels'] = (",".join(issue.get('labels'))
                       if isinstance(issue.get('labels'), Iterable)
                       else issue.get('labels'))
    resp = req_project("put", f"issues/{issue.get('iid')}",
                       project_id=issue.get('project_id'), data=issue)
    try:
        data = resp.json()
    except Exception as e:
        raise ValueError(f"Could not patch issues: {e}")

    if not status.is_success(resp.status_code):
        if resp.status_code == status.HTTP_404_NOT_FOUND:
            raise Http404("Issue not found")
        raise ValueError(f"Could not patch issues: {data}")

    return data
