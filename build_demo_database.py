from typing import List

import names
import random
from datetime import timedelta

from django.utils import timezone
from safedelete import HARD_DELETE

from infra.models import Hospital, IntensiveCareService, Unit, Bed
from orea.models import User
from patients.models import Patient, PatientNote, TreatmentLimitation, \
    FailureMeasure, DetectionMeasure, PatientStay, BedStay
from orea.settings import IS_LOCAL_SERVER, DEMO_USER_USERNAME
from users.models import Access


# after running this code
# (using 'import build_demo_data' after `python manage.py shell`)
# you can authenticate as ckramer or mdodonna,
# affiliated to an intensive care service each

# CLEAN ########################################################################

CAN_CLEAN_DB = DEMO_USER_USERNAME is not None and len(DEMO_USER_USERNAME) > 0


def clean_db():
    if not CAN_CLEAN_DB:
        return

    [m.objects.all().delete(force_policy=HARD_DELETE) for m in [
        PatientNote, TreatmentLimitation, FailureMeasure, DetectionMeasure,
        BedStay, PatientStay, Patient, Bed, Unit, IntensiveCareService,
        Hospital, Access
    ]]
    User.objects.all().delete()

# INFRASTRUCTURE ###############################################################


def create_infra() -> [User, User, User, List[Bed], List[Bed]]:
    if not CAN_CLEAN_DB:
        return

    u = User.objects.create_superuser(
        username=DEMO_USER_USERNAME, first_name="Cid",
        last_name="Kramer", email="cid.kramer@garden.blb")
    hosp = Hospital.objects.create(name='Balamb')
    rea_serv = IntensiveCareService.objects.create(name="Seed", hospital=hosp)
    hosp_1_unit_1 = Unit.objects.create(name="Squall",
                                        intensive_care_service=rea_serv)
    hosp_1_unit_2 = Unit.objects.create(name="Linoa",
                                        intensive_care_service=rea_serv)

    hosp_1_beds = Bed.objects.bulk_create(Bed(unit=hosp_1_unit_1, unit_index=i)
                                   for i in range(5))
    hosp_1_beds += Bed.objects.bulk_create(Bed(unit=hosp_1_unit_2, unit_index=i)
                                    for i in range(5))

    Access.objects.create(
        user=u, intensive_care_service=rea_serv, start_datetime=timezone.now(),
        end_datetime=timezone.now() + timedelta(days=182),
        status=Access.Status.validated, reviewed_by=u, right_edit=True,
        right_review=True
    )
    # u.authorized_intensive_care_services.add(rea_serv)

    u_3 = User.objects.create_user(
        username="norg", first_name="Nicolas",
        last_name="Org", email="nicolas.org@garden.blb")
    Access.objects.create(
        user=u_3, intensive_care_service=rea_serv, start_datetime=timezone.now(),
        end_datetime=timezone.now() + timedelta(days=182),
        status=Access.Status.validated, reviewed_by=u, right_edit=True,
        right_review=False
    )
    # u_3.authorized_intensive_care_services.add(rea_serv)

    u_2 = User.objects.create_user(
        username="mdodonna", first_name="Martine",
        last_name="Dodonna", email="martine.dodonna@garden.gbd")
    hosp_2 = Hospital.objects.create(name='Galbadia')
    rea_serv_2 = IntensiveCareService.objects.create(
        name="Deling", hospital=hosp_2)
    hosp_2_unit_1 = Unit.objects.create(name="Irvine",
                                        intensive_care_service=rea_serv_2)
    hosp_2_unit_2 = Unit.objects.create(name="Selphie",
                                        intensive_care_service=rea_serv_2)
    hosp_2_beds = Bed.objects.bulk_create(
        Bed(unit=hosp_2_unit_1, unit_index=i) for i in range(5))
    hosp_2_beds += Bed.objects.bulk_create(
        Bed(unit=hosp_2_unit_2, unit_index=i) for i in range(5))

    Access.objects.create(
        user=u_2, intensive_care_service=rea_serv_2,
        start_datetime=timezone.now(),
        end_datetime=timezone.now() + timedelta(days=182),
        status=Access.Status.validated, reviewed_by=u, right_edit=True,
        right_review=True
    )
    # u_2.authorized_intensive_care_services.add(rea_serv_2)

    # various accesses for demo
    Access.objects.bulk_create([
        Access(
            user=us, intensive_care_service=s,
            start_datetime=(timezone.now()
                            + timedelta(days=random.choice(range(-5, 5)))),
            end_datetime=timezone.now() + timedelta(days=30), status=st,
            reviewed_by=None if st == Access.Status.created
            else (u if s.pk == rea_serv.pk else u_2),
            right_edit=True
    ) for [us, s, st] in [
            [u, rea_serv_2, Access.Status.created],
            [u, rea_serv_2, Access.Status.denied],
            [u_3, rea_serv_2, Access.Status.denied],
            [u_2, rea_serv, Access.Status.created],
            [u_2, rea_serv, Access.Status.denied],
        ]
    ])

    return u, u_2, u_3, hosp_1_beds, hosp_2_beds


# PATIENTS #####################################################################


def rand_td(nb_days_max):
    return timedelta(days=random.randint(0, nb_days_max),
                     hours=random.randint(0, 24), minutes=random.randint(0, 60))


random_rdh_notes = [
    "### 28 Mars"
    "\n\n- Intubation orotrachéale (cormack 1 au glidescope go)"
    "\n- 12 séances de décubitus ventral, dernière le **28/02**"
    "\n\n### 02 Avril"
    "\n\n- NO et Vectarion",
    "### 21 Mars"
    "\nDébut des symptômes."
    "\n\n### 28 Mars"
    "\nSAU KB 28/03 : 85% SpO2, PCR Covid +, TDM Covid +. Pneumo KB puis SSPI "
    "devant oxygénorequérance."
    "\n\n### 02 Avril"
    "\nIOT **02/04**.",
    "### 28 Mars"
    "\n- Intubation orotrachéale (cormack 1 au glidescope go)"
    "\n\n### 30 Mars"
    "\n- 12 séances de décubitus ventral, dernière le **28/04**"
    "\n\n### 02 Avril"
    "\n- NO et Vectarion",
]

random_evolution_notes = [
    "### 23 Avril\n\n"
    "- PAVM à H.Influenzae sous TIENAM puis BACTRIM jusqu'au **22/04**. 3 "
    "nouvelles séances de DV."
    "\n- CVVHD à partir du **04/04**."
    "\n\n### 29 Avril"
    "\n\n- Dernière séance de DV le **28/04** ; arrêt des curares le **29/04**."
    "\n\n### 02 Mai"
    "\n- Faux anévrysme fémoral gauche traité par pansement compressif."
    "\n\n### 05 Mai"
    "\n\n- Nouveau choc septique avec bactériémies à SAMS (Hc du **02, "
    "03, 04 et 05/05**) sous Levofloxacine et Bactrim. "
    "\nLBA 02/05+ SAMS, KTC JIG+ SAMS. ETO négative.",
    "",
]

random_todo_lists = [
    "- [x] TDM\r\n- [x] Écho/Doppler vasculaire",
    "- [x] TDM cérébrale\r\n- [x] Trachéotomie 13/05\r\n- [ ] VVP",
    "",
    "- [x] TDM\r\n- [x] Écho/Doppler vasculaire",
]

random_treatment_limitation_notes = [""]

random_day_notice_notes = [""]

random_general_notes = [
    "\n\nPassage en aide"
    "\n\nReprise de diureses aminés sevrés y a 3 jours"
    "\nSédation en diminution."
    "\n\n38°C hier pdp neg au direct.",
    "\n\n58 ans obèse bpco gold II et polyvasculaire"
    "\n\nJ2 d'un choc septique sur angiocholite"
    "\nDrainage radiologique => KP et E.faecalis sauvage, Tazo, "
    "NAD stable à 4mg/h"
    "\n\nSurveiller critères d'EER",
    "\n\n47 ans, 50 années tabac, BPCO gold IV"
    "\n\nProjet transplant+, exacerbation sur grippe A"
    "\nph 7.16 et CO2=92 à l'admission"
    "\n\nJ8 amélioration lente, pH 7.31 et PCO2 64, autoPEP à 6, "
    "toujours curarisé",
]

random_hospitalisation_causes = [
    "Pneumonie à Covid-19",
    "Détresse respiratoire aigue",
    "SDRA",
    "Intoxication médicamenteuse multiple",
    "Inhalation sur syndrome occlusif",
    "Coma sur TC",
    "Post-opératoire syndrome occlusif sur colectasie majeure",
    "Choc hémorragique sur HPP",
    "Hypothermie thérapeutique",
    "Arrêt cardiaque ischemique",
]

random_antecedents = [
    ('Autre', "Obésité morbide IMC > 43 kg/m²"),
    ('Cardiaque', "Arythmie paroxystique"),
    ('Psychiatrique', "Trouble bipolaire"),
    ('Obésité', ""),
]

random_allergies = [
    ("Non indiqué", "")
]


def create_random_patient(bed: Bed, creator: User, updaters: List[User]):
    p = Patient.objects.create(
        local_id=str(random.randint(0, 10000)),
        first_name=names.get_first_name(),
        last_name=names.get_last_name(),
        birth_date=timezone.now() - timedelta(days=random.randint(10 * 365,
                                                                  80 * 365)),
        sex=random.choice(Patient.PatientSex.values),
        size_cm=random.randint(120, 200),
        weight_kg=random.randint(40, 120),
        antecedents=dict(random.choices(
            random_antecedents, k=random.randint(0, len(random_antecedents)))),
        allergies=dict(random.choices(
            random_allergies, k=random.randint(0, len(random_allergies)))),
    )
    days_in_stay = random.randint(0, 20)
    s = PatientStay.objects.create(
        patient=p,
        hospitalisation_cause=random.choice(random_hospitalisation_causes),
        severity=random.choice(PatientStay.StaySeverity.values),
    )
    BedStay.objects.create(
        patient_stay=s,
        bed=bed,
        start_date=timezone.now() - timedelta(days=days_in_stay),
    )

    FailureMeasure.objects.bulk_create([FailureMeasure(
        created_by=creator,
        measure_datetime=timezone.now() - rand_td(days_in_stay),
        stay=s, failure_type=t, active=random.random() > 0.5,
        indication="Additional info"
    ) for t in FailureMeasure.FailureType.values if random.random() > 0.5])

    TreatmentLimitation.objects.create(
        stay=s,
        created_by=creator,
        measure_datetime=timezone.now() - rand_td(days_in_stay),
        no_acr_reanimation=random.random() < 0.1,
        no_new_failures_or_therap_raise_treatment=random.random() < 0.1,
        no_catecholamines=random.random() < 0.1,
        no_intubation=random.random() < 0.1,
        no_assisted_ventilation=random.random() < 0.1,
        no_o2=random.random() < 0.1,
        no_eer=random.random() < 0.1,
        no_transfusion=random.random() < 0.1,
        no_surgery=random.random() < 0.1,
        no_PIC_or_DVE=random.random() < 0.1,
        no_new_antibiotherapy=random.random() < 0.1,
        no_sirurgical_reintervetion=random.random() < 0.1,
        no_new_complementary_exams=random.random() < 0.1,
        no_biological_results=random.random() < 0.1,
        pressor_amines_stop=random.random() < 0.1,
        eer_stop=random.random() < 0.1,
        fio2_21percent=random.random() < 0.1,
        oxygenotherapy_stop=random.random() < 0.1,
        mecanic_ventilation_stop=random.random() < 0.1,
        extubation=random.random() < 0.1,
        nutrition_stop=random.random() < 0.1,
        hydratation_stop=random.random() < 0.1,
        antibiotic_stop=random.random() < 0.1,
        dve_ablation=random.random() < 0.1,
        ecmo_stop=random.random() < 0.1,
        all_current_therapeutics_stop=random.random() < 0.1,
        no_mecanic_ventilation_markup=random.random() < 0.1,
        no_reanimation_admittance=random.random() < 0.1,
        other_stops="Something" if random.random() < 0.1 else "",
        fio2_limit=(random.random() + 0.4) if random.random() < 0.1 else None,
        amines_limitation=((10 + 10 * random.random())
                           if random.random() < 0.1 else None),
        treatment_limitations_comments=("No comment"
                                        if random.random() < 0.1 else "")
    )

    DetectionMeasure.objects.create(
        created_by=creator, stay=s,
        measure_datetime=timezone.now() - rand_td(days_in_stay),
        blse=random.random() > 0.9,
        sarm=random.random() > 0.9,
        bhc=random.random() > 0.9,
        clostridium=random.random() > 0.9,
    )

    PatientNote.objects.bulk_create([PatientNote(
        stay=s, created_by=random.choice(updaters),
        updated_at=timezone.now() - rand_td(days_in_stay),
        type=t, content=c
    ) for (t, c) in [
        (
            PatientNote.NoteType.recent_disease_history,
            random.choice(random_rdh_notes)
        ), (
            PatientNote.NoteType.evolution,
            random.choice(random_evolution_notes)
        ), (
            PatientNote.NoteType.todo_list,
            random.choice(random_todo_lists)
        ), (
            PatientNote.NoteType.treatment_limitations,
            random.choice(random_treatment_limitation_notes)
        ), (
            PatientNote.NoteType.day_notice,
            random.choice(random_day_notice_notes)
        ),
    ]])

    PatientNote.objects.bulk_create([PatientNote(
        stay=s, created_by=u,
        updated_at=timezone.now() - rand_td(days_in_stay),
        type=PatientNote.NoteType.general, content=c
    ) for (u, c) in [
        (u, random.choice(random_general_notes))
        for u in updaters if random.random() < 0.7]
    ])


def create_patients(u: User, u_2: User, u_3: User, hosp_1_beds: List[Bed],
                    hosp_2_beds: List[Bed]):
    if not CAN_CLEAN_DB:
        return

    for b, filled in zip(hosp_1_beds,
                         [random.random() < 0.8 for _ in hosp_1_beds]):
        if filled:
            create_random_patient(b, u, [u, u_3])

    for b, filled in zip(hosp_2_beds,
                         [random.random() < 0.8 for _ in hosp_2_beds]):
        if filled:
            create_random_patient(b, u_2, [u_2])


def reset_demo():
    if not CAN_CLEAN_DB:
        return

    clean_db()
    u, u_2, u_3, beds_1, beds_2 = create_infra()
    create_patients(u, u_2, u_3, beds_1, beds_2)
