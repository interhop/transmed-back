class LoginError(Exception):
    pass


class ServerError(Exception):
    pass


class JwtTokens:
    def __init__(self, access: str, refresh: str, last_connection: dict = None,
                 **kwargs):
        self.access = access
        self.refresh = refresh
        self.last_connection = last_connection if last_connection else {}
