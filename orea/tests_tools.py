from __future__ import annotations
import json
import random
import string
import uuid
from typing import List, Union

from django.conf import settings
from django.core.handlers.wsgi import WSGIRequest
from django.db.models import Manager, Field, Model
from django.test import TestCase, override_settings
import names
from rest_framework.response import Response
from rest_framework.test import APIRequestFactory, force_authenticate, APIClient
from safedelete.models import SafeDeleteModel

from .models import BaseModel, User
from .settings import SAFE_DELETE_FIELD_NAME
from .utils import prettify_json, prettify_dict


class ObjectView(object):
    def __init__(self, d):
        self.__dict__ = d


class PagedResponse:
    def __init__(self, response: Response):
        result = json.loads(response.content)

        if 'count' not in result:
            raise Exception('No count in response')
        if not isinstance(result['count'], int):
            raise Exception('"count" in response is not integer')
        self.count = result['count']

        if 'next' not in result:
            raise Exception('No next in response')
        if not isinstance(result['next'], str) and result['next'] is not None:
            raise Exception('"next" in response is not string')
        self.next = result['next']

        if 'previous' not in result:
            raise Exception('No previous in response')
        if not isinstance(result['previous'], str)\
                and result['previous'] is not None:
            raise Exception('"previous" in response is not str')
        self.previous = result['previous']

        if 'results' not in result:
            raise Exception('No results in response')
        if not isinstance(result['results'], list):
            raise Exception('"results" in response is not list')
        self.results = result['results']


def new_random_user(
        firstname: str = '', lastname: str = '',
        username: str = '', email: str = '',
        is_staff: bool = False, password: str = ''
) -> User:
    firstname: str = firstname or names.get_first_name()
    lastname: str = lastname or names.get_last_name()
    username = username or f"{firstname.lower()[0]}{lastname.lower()}"
    email = email or f"{firstname.lower()}.{lastname.lower()}@test.com"

    u: User = User.objects.create(
        username=username,
        first_name=firstname,
        last_name=lastname,
        email=email,
        is_staff=is_staff
    )
    if password:
        u.set_password(password)
        u.save()
    return u


class CaseRetrieveFilter:
    """
    Dictionary, used in a queryset, to retrieve the item that was created
    """
    def __init__(self, exclude: dict = None, **kwargs):
        self.exclude = exclude or dict()

    @property
    def args(self) -> dict:
        return dict([(k, v) for (k, v)
                     in self.__dict__.items() if k != 'exclude'])


class RequestCase:
    """
    General representation of a test case
    """
    def __init__(self, status: int, success: bool, user: User = None,
                 title: str = "", **kwargs):
        self.title = title
        self.user = user
        self.status = status
        self.success = success

    def clone(self, **kwargs) -> RequestCase:
        return self.__class__(**{**self.__dict__, **kwargs})

    @property
    def description_dict(self) -> dict:
        d = {
            **self.__dict__,
            'user': self.user and str(self.user),
        }
        d.pop('title', None)
        return d

    @property
    def description(self):
        return f"Case {self.title}: {prettify_dict(self.description_dict)}"

    def __str__(self):
        return self.title


class CreateCase(RequestCase):
    """
    General representation of a test case for Creating an object
    """
    retrieve_filter: CaseRetrieveFilter

    def __init__(self, data: dict, retrieve_filter: CaseRetrieveFilter,
                 **kwargs):
        super(CreateCase, self).__init__(**kwargs)
        self.data = data
        self.retrieve_filter = retrieve_filter

    @property
    def json_data(self) -> dict:
        d = self.data.copy()
        return d


class RetrieveCase(RequestCase):
    """
    General representation of a test case for Reading a specific object
    """
    def __init__(self, to_find: any = None, url: str = "", params: dict = None,
                 view_params: dict = None, **kwargs):
        super(RetrieveCase, self).__init__(**kwargs)
        self.to_find = to_find
        self.url = url
        self.params = params or {}
        self.view_params = view_params or dict()

    @property
    def description_dict(self) -> dict:
        d = {
            **super(RetrieveCase, self).description_dict,
            'to_find': str(self.to_find)
        }
        return d

    def clone(self, **kwargs) -> RetrieveCase:
        return self.__class__(**{**self.__dict__, **kwargs})


class ListCase(RequestCase):
    """
    General representation of a test case for Reading a list of objects
    """
    def __init__(self, to_find: list = None, url: str = "",
                 page_size: int = None, params: dict = None,
                 previously_found_ids: List = None, **kwargs):
        super(ListCase, self).__init__(**kwargs)
        self.to_find = to_find or []
        self.url = url
        self.page_size = page_size if page_size is not None \
            else settings.REST_FRAMEWORK.get('PAGE_SIZE')
        self.params = params or {}
        self.previously_found_ids = previously_found_ids or []

    @property
    def description_dict(self) -> dict:
        d = {
            **super(ListCase, self).description_dict,
            'previously_found_ids': [],
            'to_find': ([str(obj) for obj in self.to_find[0:10]]
                        + (["..."] if len(self.to_find) > 10 else [])),
        }
        return d

    def clone(self, **kwargs) -> ListCase:
        return self.__class__(**{**self.__dict__, **kwargs})


class ActionCase(RequestCase):
    """
    General representation of a test case for Updating partially an object
    """
    def __init__(self, initial_data: dict, **kwargs):
        super(ActionCase, self).__init__(**kwargs)
        self.initial_data = initial_data


class PatchCase(ActionCase):
    """
    General representation of a test case for Updating partially an object
    """
    def __init__(self, data_to_update: dict, **kwargs):
        super(PatchCase, self).__init__(**kwargs)
        self.data_to_update = data_to_update


class DeleteCase(RequestCase):
    """
    General representation of a test case for Deleting an object
    """
    def __init__(self, data_to_delete: dict, **kwargs):
        super(DeleteCase, self).__init__(**kwargs)
        self.data_to_delete = data_to_delete


@override_settings(ADMINS=None)
class BaseTests(TestCase):
    """
    Tests class useful for testing results from CRUD requests on a BaseModel
    """
    # fields from a model that cannot be updated after creation
    unupdatable_fields = []
    # fields from a model that cannot be set on creation
    unsettable_fields = []
    # fields from a model that cannot be set on creation and have a default
    unsettable_default_fields = {}
    # fields sent in requests that have are tested to a different model
    # field/param name
    alias_fields = {}

    def setUp(self):
        self.client = APIClient()
        self.factory = APIRequestFactory()

    def get_response_payload(self, response):
        response.render()
        return json.loads(response.content)

    def check_is_created(self, result: BaseModel,
                         request_model: dict = None, **kwargs):
        request_model = request_model or dict()
        self.assertIsNotNone(result)
        self.assertTrue(isinstance(result, BaseModel))

        [
            self.assertEqual(
                getattr(result, f), v,
                f"Error, model's {f} should have been set to a default."
            ) for [f, v] in self.unsettable_default_fields.items()
        ]

        if len(request_model.items()) > 0:
            [
                self.assertNotEqual(
                    getattr(result, f), request_model.get(f),
                    f"Error, model's {f} should not have been set."
                ) for f in self.unsettable_fields if f in request_model
            ]

    def check_is_deleted(self, base_instance: any):
        if isinstance(base_instance, SafeDeleteModel):
            self.assertIsNotNone(base_instance)
            self.assertIsNotNone(getattr(base_instance, SAFE_DELETE_FIELD_NAME))
        else:
            self.assertIsNone(base_instance)

    def check_is_updated(self, result: BaseModel, patch_case: PatchCase,
                         origin: BaseModel):
        self.assertTrue(isinstance(result, Model))
        self.assertTrue(isinstance(origin, Model))
        for f in patch_case.data_to_update.keys():
            new_value = getattr(result, self.alias_fields.get(f, f))
            req_value = patch_case.data_to_update.get(f)
            origin_value = getattr(origin, self.alias_fields.get(f, f))

            # in case of PK related field
            new_value = getattr(new_value, 'pk', new_value)
            origin_value = getattr(origin_value, 'pk', origin_value)

            # in case of uuid PK
            new_value = (str(new_value)
                         if isinstance(new_value, uuid.UUID)
                         else new_value)
            req_value = (str(req_value)
                         if isinstance(req_value, uuid.UUID)
                         else req_value)
            origin_value = (str(origin_value)
                            if isinstance(origin_value, uuid.UUID)
                            else origin_value)

            if f in self.unupdatable_fields:
                self.assertEqual(
                    new_value, origin_value,
                    f"Error, field '{f}' was updated: was '{origin_value}', "
                    f"now is '{new_value}'. Case : {patch_case.description}")
            else:
                self.assertEqual(
                    new_value, req_value,
                    f"Error, field '{f}' was not updated: was '{origin_value}',"
                    f" expected '{patch_case.data_to_update.get(f)}', "
                    f"but is '{new_value}'. Case : {patch_case.description}")

    def check_is_not_updated(self, result: BaseModel,
                             patch_case: PatchCase, origin: BaseModel):
        self.assertTrue(isinstance(result, Model))
        self.assertTrue(isinstance(origin, Model))
        for f in patch_case.data_to_update.keys():
            new_value = getattr(result, self.alias_fields.get(f, f))
            origin_value = getattr(origin, self.alias_fields.get(f, f))

            # in case of PK related field
            new_value = getattr(new_value, 'pk', new_value)
            origin_value = getattr(origin_value, 'pk', origin_value)

            # in case of uuid PK
            new_value = (str(new_value)
                         if isinstance(new_value, uuid.UUID)
                         else new_value)
            origin_value = (str(origin_value)
                            if isinstance(origin_value, uuid.UUID)
                            else origin_value)

            self.assertEqual(
                new_value, origin_value,
                f"Error, field '{f}' was updated: was '{origin_value}', "
                f"now is '{new_value}'. Case : {patch_case.description}")


class ViewSetTests(BaseTests):
    """
    Tests class for basic requests cases on a Rest ViewSet
    """
    objects_url: str
    create_view: any
    update_view: any
    delete_view: any
    list_view: any
    retrieve_view: any
    model: Model
    model_objects: Manager
    model_fields: List[Field]

    def check_create_case(self, case: CreateCase, other_view: any = None,
                          **view_kwargs) -> Union[BaseModel, None]:
        request = self.factory.post(self.objects_url, case.json_data,
                                    format='json')
        if case.user:
            force_authenticate(request, case.user)

        response = other_view(request, **view_kwargs) if other_view else \
            self.__class__.create_view(request)
        response.render()

        self.assertEqual(
            response.status_code, case.status,
            msg=(f"{case.description}"
                 + (f" -> {prettify_json(response.content)}"
                    if response.content else "")),
        )

        inst = self.model_objects.filter(**case.retrieve_filter.args)\
            .exclude(**case.retrieve_filter.exclude).first()

        if case.success:
            self.check_is_created(inst, request_model=case.data, user=case.user)
        else:
            self.assertIsNone(inst, f"Object was created: {inst}. "
                                    f"{case.description}")
        return inst

    def check_delete_case(self, case: DeleteCase, obj: Model = None):
        if obj is None:
            obj = self.model_objects.create(**case.data_to_delete)

        request = self.factory.delete(self.objects_url)
        force_authenticate(request, case.user)
        response = self.__class__.delete_view(
            # request, **{self.model._meta.pk.name: obj_id}
            request, pk=obj.pk
        )
        response.render()

        self.assertEqual(
            response.status_code, case.status,
            msg=(f"{case.description}"
                 + (f" -> {prettify_json(response.content)}"
                    if response.content else "")),
        )

        if isinstance(self.model(), BaseModel):
            obj = self.model.all_objects.filter(pk=obj.pk).first()
        else:
            obj = self.model_objects.filter(pk=obj.pk).first()

        if case.success:
            self.check_is_deleted(obj)
        else:
            self.assertIsNotNone(obj)
            if isinstance(self.model, BaseModel):
                self.assertIsNone(obj.delete_datetime)
            try:
                obj.delete()
            except Exception:
                pass

    def check_patch_case(self, case: PatchCase, obj: Model = None,
                         request: WSGIRequest = None):
        if obj is None:
            obj_id = self.model_objects.create(**case.initial_data).pk
            obj = self.model_objects.get(pk=obj_id)

        if request is None:
            request = self.factory.patch(
                self.objects_url, case.data_to_update, format='json'
            )
            force_authenticate(request, case.user)

        response = self.__class__.update_view(
            # request, **{self.model._meta.pk.name: obj_id}
            request, pk=obj.pk
        )
        response.render()

        self.assertEqual(
            response.status_code, case.status,
            msg=(f"{case.description}"
                 + (f" -> {prettify_json(response.content)}"
                    if response.content else "")),
        )

        new_obj = self.model_objects.filter(pk=obj.pk).first()

        if case.success:
            self.check_is_updated(new_obj, case, obj)
        else:
            self.check_is_not_updated(new_obj, case, obj)

    def check_get_paged_list_case(
            self, case: ListCase, other_view: any = None, **view_kwargs):
        request = self.factory.get(
            path=case.url or self.objects_url,
            data=[] if case.url else case.params)
        force_authenticate(request, case.user)
        response = other_view(request, **view_kwargs) if other_view else \
            self.__class__.list_view(request)

        response.render()
        self.assertEqual(
            response.status_code, case.status,
            msg=(f"{case.title}: "
                 + prettify_json(response.content) if response.content else ""),
        )

        if not case.success:
            self.assertNotIn('results', response.data, f"Case {case.title}")
            return

        res = PagedResponse(response)

        self.assertEqual(res.count, len(case.to_find), case.description)

        obj_to_find_ids = [str(obj.pk) for obj in case.to_find]
        current_obj_found_ids = [obj.get(self.model._meta.pk.name)
                                 for obj in res.results]
        obj_found_ids = current_obj_found_ids + case.previously_found_ids

        if case.page_size is not None:
            if res.next:
                self.assertEqual(
                    len(current_obj_found_ids), case.page_size
                )
                next_case = case.clone(
                    url=res.next, previously_found_ids=obj_found_ids)
                self.check_get_paged_list_case(next_case, other_view,
                                               **view_kwargs)
        else:
            self.assertCountEqual(map(str, obj_found_ids),
                                  map(str, obj_to_find_ids))

    def check_retrieve_case(self, case: RetrieveCase):
        request = self.factory.get(
            path=case.url or self.objects_url,
            data=[] if case.url else case.params)
        force_authenticate(request, case.user)
        response = self.__class__.retrieve_view(request, **case.view_params)
        response.render()
        self.assertEqual(
            response.status_code, case.status,
            msg=(f"{case.title}: "
                 + prettify_json(response.content) if response.content else ""),
        )
        res: dict = response.data

        if case.success:
            if case.to_find is not None:
                self.assertEqual(str(case.to_find.pk),
                                 str(res.get(self.model._meta.pk.name)),
                                 case.description)
            else:
                self.assertEqual(len(res), 0, case.description)


def random_str(length, include_pattern: str = '', with_space: bool = True):
    letters = string.ascii_lowercase + (" " if with_space else "")
    res = ''.join(random.choice(letters) for i in range(length))
    if include_pattern:
        h = int(length / 2)
        res = f"{res[:h]}{include_pattern}{res[h:]}"
    return res
