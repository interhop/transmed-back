import json

from dj_rest_auth.app_settings import create_token, JWT_AUTH_REFRESH_COOKIE
from dj_rest_auth.jwt_auth import get_refresh_view
from dj_rest_auth.models import get_token_model
from dj_rest_auth.views import LoginView, LogoutView
import django_filters
from django.conf import settings
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django_filters import FilterSet, filters
from djangorestframework_camel_case.util import camelize

from rest_framework import viewsets, status
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAdminUser, SAFE_METHODS, \
    IsAuthenticatedOrReadOnly, OR
from rest_framework_tracking.mixins import LoggingMixin
from rest_framework_tracking.models import APIRequestLog

from . import __version__
from .models import User
from .permissions import IsSelf, IsAdminOrReadOnly
from .serializers import APIRequestLogSerializer, UserSerializer, \
    BackendStateSerializer, CustomRefreshTokenSerializer, NonAdminUserSerializer
from .settings import ALLOW_ACCOUNT_MANAGEMENT, DEMO_USER_USERNAME
from .types import ServerError


# todo : add only diff to logs when updating textfield
# https://miguendes.me/python-compare-strings
class CustomLoggingMixin(LoggingMixin):
    logging_methods = ['POST', 'PUT', 'PATCH', 'DELETE']

    def handle_log(self):
        for f in ['data', 'errors', 'response']:
            v = self.log.get(f, None)
            if isinstance(v, dict) or isinstance(v, list):
                try:
                    self.log[f] = json.dumps(v)
                except Exception:
                    pass

        return super(CustomLoggingMixin, self).handle_log()


class BaseFilterSet(FilterSet):
    def get_uuid(self, queryset, field, value):
        return queryset.filter(
            **{f'{field}__in': str(value).upper().split(",")}
        )
    uuid = filters.CharFilter(method="get_uuid")


class BaseViewSet(CustomLoggingMixin, viewsets.ModelViewSet):
    nested_serializer_class = None
    http_method_names = ["get", "delete", "post", "patch"]

    def get_serializer_context(self):
        return {'request': self.request}

    def get_serializer_class(self):
        if (self.nested_serializer_class is not None
                and 'full' in self.request.query_params
                and str(self.request.query_params.get('full')).lower() == "true"
                and self.request.method in SAFE_METHODS):
            return self.nested_serializer_class
        else:
            return super(BaseViewSet, self).get_serializer_class()


class BackendStateView(RetrieveAPIView):
    """
    Describes, for the Frontend to adapt, the state of the Backend
    (version, what can be called, etc.)
    """
    serializer_class = BackendStateSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)
    http_method_names = ('get',)

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer({
            'version': __version__,
            'can_manage_accounts': ALLOW_ACCOUNT_MANAGEMENT,
            'demo_username': DEMO_USER_USERNAME or None
        }, many=False)
        return JsonResponse(camelize(serializer.data))


def get_custom_refresh_view():
    """
    Does the same as dj_rest_auth.get_refresh_view but adding possibility
    to call one other, dedicated jwt server, using orea.conf_auth,
    via CustomRefreshTokenSerializer
    """
    refresh_view = get_refresh_view()
    refresh_view.serializer_class = CustomRefreshTokenSerializer
    return refresh_view


class UserFilter(BaseFilterSet):
    def bed_filter(self, queryset, field, value):
        return queryset.filter(
            **{f'{field}__in': str(value).upper().split(",")}
        ).distinct()

    authorized_reanimation_services__unit__bed = django_filters.CharFilter(
        method="bed_filter")

    class Meta:
        model = User
        fields = "__all__"


class UserViewSet(BaseViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    http_method_names = ("get", "post", "patch",)
    ordering_fields = (
        "first_name",
        "last_name",
        "username",
        "email",
    )
    search_fields = ["first_name", "last_name", "username", "email"]

    def get_permissions(self):
        return [OR(IsAdminOrReadOnly(), IsSelf())]

    def get_serializer_class(self):
        user: User = self.request.user
        if user.is_staff:
            return super().get_serializer_class()
        else:
            return NonAdminUserSerializer

    def get_queryset(self):
        q = super(UserViewSet, self).get_queryset()
        user = self.request.user
        if user.is_staff or user.is_superuser:
            return q

        authorized_reas = user.authorized_intensive_care_services.all()
        return (q.filter(
            accesses__intensive_care_service__in=authorized_reas
        ) | q.filter(
            pk=user.pk
        )).distinct()


class LogFilter(django_filters.FilterSet):
    def method_filter(self, queryset, field, value):
        return queryset.filter(
            **{f'{field}__in': str(value).upper().split(",")}
        )

    def status_code_filter(self, queryset, field, value):
        return queryset.filter(
            **{f'{field}__in': [int(v) for v in str(value).upper().split(",")]}
        )

    method = django_filters.CharFilter(method='method_filter')
    status_code = django_filters.CharFilter(method='status_code_filter')
    requested_at = django_filters.DateTimeFromToRangeFilter()
    response_ms = django_filters.RangeFilter()
    path_contains = django_filters.CharFilter(
        field_name='path', lookup_expr='icontains'
    )
    response_contains = django_filters.CharFilter(
        field_name='response', lookup_expr='icontains'
    )
    errors_contains = django_filters.CharFilter(
        field_name='errors', lookup_expr='icontains'
    )
    data_contains = django_filters.CharFilter(
        field_name='data', lookup_expr='icontains'
    )

    class Meta:
        model = APIRequestLog
        fields = [f.name for f in APIRequestLog._meta.fields] + [
            'path_contains',
            'response_contains',
            'errors_contains',
            'data_contains',
        ]


class LoggingViewset(viewsets.ModelViewSet):
    queryset = APIRequestLog.objects.all()
    serializer_class = APIRequestLogSerializer
    filter_class = LogFilter
    ordering_fields = ("path", "response", "errors", "data",)
    search_fields = ("path", "response", "errors", "data",)

    permission_classes = (IsAdminUser, )
    http_method_names = ("get",)


class CustomLoginView(LoginView):
    def login(self):
        self.user = self.serializer.validated_data['user']
        token_model = get_token_model()

        if getattr(settings, 'REST_USE_JWT', False):
            # overwrote jwt_encode(user) line into orea.authentication backend
            # in order to make it easier to gather auth one other, dedicated
            # jwt server
            self.access_token = self.request.jwt_session_key
            self.refresh_token = self.request.jwt_refresh_key
        elif token_model:
            self.token = create_token(token_model, self.user, self.serializer)

        if getattr(settings, 'REST_SESSION_LOGIN', True):
            self.process_login()

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        try:
            resp = super(CustomLoginView, self).post(request, *args, **kwargs)
            return resp
        except ServerError as e:
            # can be thrown by conf_auth's check_ids method
            return HttpResponse(
                status=status.HTTP_503_SERVICE_UNAVAILABLE,
                content=e
            )


class CustomLogoutView(LogoutView):
    """
    Duplication of dj_rest_auth only in order to fix issue 261
    https://github.com/iMerica/dj-rest-auth/issues/261
    """
    def logout(self, request):
        if 'refresh' not in request.data:
            if JWT_AUTH_REFRESH_COOKIE:
                refresh = request.COOKIES.get(JWT_AUTH_REFRESH_COOKIE)
                if refresh:
                    request.data.update(refresh=refresh)
        return super().logout(request)
