from datetime import timedelta
import json
from typing import Union

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError


def prettify_dict(obj: Union[dict, list]) -> str:
    return json.dumps(obj, sort_keys=True, indent=2, default=str)


def prettify_json(str_json: str) -> str:
    try:
        return prettify_dict(json.loads(str_json))
    except Exception:
        return str_json


class NoFutureValidator:
    message = _('date cannot be in the future')
    requires_context = True

    def __call__(self, attrs, serializer):
        if attrs is not None and (
                attrs > (timezone.now() + timedelta(minutes=1))
        ):
            raise ValidationError(self.message)


class NoFutureDateTimeField(serializers.DateTimeField):
    def get_validators(self):
        return [*super(NoFutureDateTimeField, self).get_validators(),
                NoFutureValidator()]
