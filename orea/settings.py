import os
from datetime import timedelta

import pytz

import environ
from django.utils.translation import gettext_lazy as _

from . import __version__, __title__

env = environ.Env(
    IS_LOCAL_SERVER=(bool, False),
    DEBUG=(bool, False),
    VOTING_ENABLE=(bool, False),
    ALLOW_ACCOUNT_MANAGEMENT=(bool, False),
    TEST_SERVER=(bool, False),
    DEFAULT_AUTH_ROTATE_REFRESH_TOKEN=(bool, True)
)
environ.Env.read_env()

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('SECRET_KEY')  # , default='secret')

# allows to tell if you're running safely on your local machine
# (ex: no needs for real password when running locally)
IS_LOCAL_SERVER = env('IS_LOCAL_SERVER', default=True)
DEMO_USER_USERNAME = env('DEMO_USER_USERNAME', default=None)
# use it for differencing dev, qualif, prod environments
ENVIRONMENT = env('ENVIRONMENT', default="dev")
BACK_URL = env('BACK_URL', default="localhost")
# urls that will be allowed to call this backend
FRONT_URLS = env.list('FRONT_URLS', default=[])
BASE_APP_FOLDER = "orea"

# SECURITY WARNING: don't run with debug turned on in production!
# Debug will also send sensitive data with the response to an error
DEBUG = IS_LOCAL_SERVER or ENVIRONMENT == "dev"
# django will send email for error when DEBUG=False
ADMINS = [admin.split(";") for admin in env.list('ADMINS', default="")]

# URLS, CORS AND CSRF ##########################################################

SERVER_IP = env('BACK_SERVER_NAME', default="127.0.0.1")  # localhost:8000
if IS_LOCAL_SERVER:
    CORS_ORIGIN_WHITELIST = ["http://localhost:3000",  # react run start
                             f"http://{SERVER_IP}"]  # for django web interface
    CSRF_TRUSTED_ORIGINS = ["http://localhost:3000",
                            f"http://{SERVER_IP}"]

else:
    CORS_ORIGIN_WHITELIST = [f"http://{u}" for u in [BACK_URL] + FRONT_URLS]
    CSRF_TRUSTED_ORIGINS = [f"http://{u}" for u in [BACK_URL] + FRONT_URLS]

CORS_ALLOW_HEADERS = [
    'access-control-allow-origin',
    'content-type',
    'Authorization',
    'X-CSRFToken',
]

if IS_LOCAL_SERVER:
    ALLOWED_HOSTS = [BACK_URL, "localhost"]
else:
    ALLOWED_HOSTS = [BACK_URL]

utc = pytz.UTC

# APPLICATION DEFINITION #######################################################

INCLUDED_APPS = [BASE_APP_FOLDER] + env.list(
    'INCLUDED_APPS', default=["infra", "patients", "users", "voting"]
)
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_extensions",

    "allauth",  # required by dj_rest_auth
    # "allauth.account",
    "corsheaders",
    "django_filters",
    "dj_rest_auth",
    "dj_rest_auth.registration",
    "drf_spectacular",
    "rest_framework",
    "rest_framework_tracking",
    'safedelete',
] + (
    ["rest_framework_simplejwt.token_blacklist"]
    if env(
        'DEFAULT_AUTH_ROTATE_REFRESH_TOKEN', default=True
    ) else []
) + INCLUDED_APPS

# checking if example_setup files for each app is present
for app, example, conf in [
    ('orea', 'example.conf_auth', 'conf_auth'),
]:
    p = os.path.join(app, f"{conf}.py")
    if app in INSTALLED_APPS and not os.path.exists(p):
        raise Exception(
            f"You want '{app}' app, but {p} file could not be found."
            f"Check {app}.{conf} to build it.")

AUTHENTICATION_BACKENDS = (
    f'{BASE_APP_FOLDER}.authentication.AuthBackend',
)
AUTH_USER_MODEL = f"{BASE_APP_FOLDER}.User"
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    "django_cprofile_middleware.middleware.ProfilerMiddleware",
]

# DJANGO_CPROFILE_MIDDLEWARE_REQUIRE_STAFF = False

# REST_FRAMEWORK ###############################################################

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        f'{BASE_APP_FOLDER}.authentication.CustomAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': [
        'django_filters.rest_framework.DjangoFilterBackend',
        'rest_framework.filters.SearchFilter'
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination'
                                '.LimitOffsetPagination',
    'DEFAULT_PARSER_CLASSES': (
        'djangorestframework_camel_case.parser.CamelCaseFormParser',
        'djangorestframework_camel_case.parser.CamelCaseMultiPartParser',
        'djangorestframework_camel_case.parser.CamelCaseJSONParser',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAdminUser',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
        'djangorestframework_camel_case.render.CamelCaseBrowsableAPIRenderer',
    ),
    'DEFAULT_SCHEMA_CLASS': f'{BASE_APP_FOLDER}.AutoSchema.CustomAutoSchema',
    'JSON_UNDERSCOREIZE': {
        'no_underscore_before_number': True,
    },
    'PAGE_SIZE': 100,
}

REST_FRAMEWORK_EXTENSIONS = {
    'DEFAULT_PARENT_LOOKUP_KWARG_NAME_PREFIX': '',
}

APPEND_SLASH = False
LOGOUT_REDIRECT_URL = '/'
ROOT_URLCONF = f'{BASE_APP_FOLDER}.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = f'{BASE_APP_FOLDER}.wsgi.application'

# DATABASE #####################################################################
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': "django.db.backends.postgresql",
        'NAME': env('POSTGRES_DB', default="orea_dev"),
        'USER': env('POSTGRES_USER', default="orea_dev"),
        'PASSWORD': env('POSTGRES_PASSWORD', default='password'),
        'HOST': env('POSTGRES_HOST', default='localhost'),
        'PORT': env('POSTGRES_PORT', default=''),
    },
    'TEST': {
        'NAME': 'test_orea',
    }
}


class DisableMigrations(object):
    def __contains__(self, item):
        return True

    def __getitem__(self, item):
        return None


if env('TEST_SERVER', False):
    # When setting the test database from scratch (in gitlab job),
    # seems difficult to create tables allauth_socialaccount, socialapp and
    # socialtoken (because created before orea_user)
    # This tip seems to prevent their creations
    MIGRATION_MODULES = DisableMigrations()

# DJANGO OPTIONS ###############################################################

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators
# in case of sign-in possibility

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': f"django.contrib.auth.password_validation.{v}"
    } for v in (
        "UserAttributeSimilarityValidator",
        "MinimumLengthValidator",
        "CommonPasswordValidator",
        "NumericPasswordValidator"
    )
]

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGES = (
    ('en', _('English')),
    ('fr', _('French')),
)
LANGUAGE_CODE = 'fr'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
LOCALE_PATHS = (os.path.join(BASE_DIR, "locale"),)

# Logs
# https://docs.djangoproject.com/en/4.1/topics/logging/

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': "{levelname} {asctime} module={module} "
                      "pid={process:d} tid={thread:d} msg=`{message}`",
            'style': "{",
        }
    },
    'handlers': {
        'console': {
            'level': "INFO",
            'class': "logging.StreamHandler",
            'formatter': "verbose"
        },
        'mail_admins': {
            'level': "ERROR",
            'class': "django.utils.log.AdminEmailHandler",
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        'django.request': {
            'handlers': ['console', 'mail_admins'],
            'level': "ERROR",
            'propagate': False,
        }
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# MODULES ######################################################################

# dj_rest_auth
JWT_AUTH_COOKIE = "access"
JWT_AUTH_REFRESH_COOKIE = "refresh"
REST_AUTH_SERIALIZERS = {
    'USER_DETAILS_SERIALIZER': 'orea.serializers.UserDetailsSerializer'
}
REST_AUTH_TOKEN_MODEL = None
REST_USE_JWT = True
SIMPLE_JWT = {
    'USER_ID_FIELD': 'pk',
    "ACCESS_TOKEN_LIFETIME": timedelta(
        minutes=env('DEFAULT_AUTH_ACCESS_TOKEN_LIFETIME_MINUTES', default=20)
    ),
    "REFRESH_TOKEN_LIFETIME": timedelta(
        days=env('DEFAULT_AUTH_REFRESH_TOKEN_LIFETIME_DAYS', default=1)
    ),
    "ROTATE_REFRESH_TOKENS": env(
        'DEFAULT_AUTH_ROTATE_REFRESH_TOKEN', default=True
    ),
    "BLACKLIST_AFTER_ROTATION": env(
        'DEFAULT_AUTH_ROTATE_REFRESH_TOKEN', default=True
    ),

    # 'ALGORITHM': "HS256",
    # 'SIGNING_KEY': SECRET_KEY,
    # 'VERIFYING_KEY': ,
    # 'AUDIENCE': ,
    # 'ISSUER': ,
    # 'JWK_URL': ,
    # 'LEEWAY': ,
    # 'JSON_ENCODER': ,
}
REST_SESSION_LOGIN = False

# docs
SPECTACULAR_SETTINGS = {
    'TITLE': __title__,
    'DESCRIPTION': 'API for Orea',
    'VERSION': __version__,
    'SERVE_INCLUDE_SCHEMA': False,
}

# safe_delete
SAFE_DELETE_FIELD_NAME = "deleted_at"

# PROJECT CONSTANTS ###########################################################

# register

ALLOW_ACCOUNT_MANAGEMENT = env('ALLOW_ACCOUNT_MANAGEMENT', default=True)
# "optional", "none"
EMAIL_VERIFICATION = env('EMAIL_VERIFICATION', default="mandatory")

# Celery, in case it is necessary to implement
# CELERY_BROKER_URL = ""  # 'redis://localhost:6380'
# CELERY_RESULT_BACKEND = ""  # 'redis://localhost:6380'
# CELERY_ACCEPT_CONTENT = ['application/json']
# CELERY_RESULT_SERIALIZER = 'json'
# CELERY_TASK_SERIALIZER = 'json'
# CELERY_TASK_ALWAYS_EAGER = False
#
# CELERY_BEAT_SCHEDULE = {
#     'task-name': {
#         'task': 'task.location.function',
#         'schedule': nb_minutes,
#     }
# }


# VOTING

VOTING_ENABLE = env('VOTING_ENABLE', default=False)
VOTING_API_URL = "https://framagit.org/api/v4"
VOTING_GROUP_ID = 194860
VOTING_PROJECT_ID = 87295
VOTING_PROJECT_NAME = "issues"
VOTING_PRIVATE_TOKEN = env('VOTING_PRIVATE_TOKEN', default="")
VOTING_AUTHORIZED_LABELS = [
    "0-Request", "1-Back", "2-Front",
    "A-To do", "B-Doing", "C-Code review", "D-Qualification", "E-Finished",
    "X-Idea", "Y-Validated", "Z-Defined",
    "i-UX", "j-User request", "k-Technical",
]
VOTING_ATTACHMENT_MAX_SIZE = 10000000
VOTING_POST_LABELS = ["j-User request", "0-Request"]
VOTING_BASE_LABELS = ["X-Idea", "j-User request"]

# CONFIG CONSTANTS ###########################################################
