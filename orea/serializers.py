from dj_rest_auth.jwt_auth import CookieTokenRefreshSerializer
from rest_framework import serializers
from rest_framework_tracking.models import APIRequestLog

from . import conf_auth
from .models import User
from .types import JwtTokens


class BaseSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(read_only=True)
    insert_datetime = serializers.DateTimeField(read_only=True)
    update_datetime = serializers.DateTimeField(read_only=True)
    delete_datetime = serializers.DateTimeField(read_only=True)


class BackendStateSerializer(serializers.Serializer):
    version = serializers.CharField(read_only=True)
    can_manage_accounts = serializers.BooleanField(read_only=True)
    demo_username = serializers.CharField(read_only=True)


class CustomRefreshTokenSerializer(CookieTokenRefreshSerializer):
    def validate(self, attrs):
        try:
            tokens: JwtTokens = conf_auth.refresh_jwt(
                self.extract_refresh_token()
            )
            return {
                'access': tokens.access,
                'refresh': tokens.refresh
            }
        except NotImplementedError:
            return super(CustomRefreshTokenSerializer, self).validate(attrs)


class UserDetailsSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    email = serializers.CharField()
    uuid = serializers.UUIDField()
    username = serializers.CharField()


class ReducedUserSerializer(serializers.Serializer):
    class Meta:
        model = User
        fields = ["user", "username"]


class APIRequestLogSerializer(serializers.ModelSerializer):
    user_details = UserDetailsSerializer(allow_null=True)

    class Meta:
        model = APIRequestLog
        fields = "__all__"
        # fields = [f.name for f in APIRequestLog._meta.fields] + [
        #     "user_details",
        # ]


class UserSerializer(BaseSerializer):
    date_joined = serializers.DateTimeField(read_only=True)

    class Meta:
        model = User
        fields = "__all__"


class NonAdminUserSerializer(BaseSerializer):
    title = serializers.CharField(read_only=True)
    is_staff = serializers.BooleanField(read_only=True)
    is_active = serializers.BooleanField(read_only=True)

    class Meta:
        model = User
        fields = "__all__"
