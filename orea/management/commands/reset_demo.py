from django.core.management import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        from build_demo_database import reset_demo
        reset_demo()
