import uuid as uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import QuerySet, Q
from django.utils.translation import gettext_lazy as _
from safedelete import SOFT_DELETE_CASCADE
from safedelete.models import SafeDeleteModel


class BaseTextChoices(models.TextChoices):
    @classmethod
    def max_length(cls):
        return max(len(v) for v in cls.values)


class BaseModel(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(AbstractUser):
    class Title(BaseTextChoices):
        doctor = "D", _("Doctor")
        physio = "P", _("Physiotherapist")
        care_giver = "CG", _("Care-giver")
        nurse = "N", _("Nurse")

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    title = models.CharField(blank=True, null=True, choices=Title.choices,
                             max_length=Title.max_length())

    @property
    def authorized_intensive_care_services(self) -> QuerySet:
        try:
            from users.models import Access
        except ImportError:
            raise ImportError('users app not implemented')
        try:
            from infra.models import IntensiveCareService
        except ImportError:
            raise ImportError('infras app not implemented')

        return IntensiveCareService.objects.filter(
            Access.is_valid_Q(prefix='accesses')
            & (Q(accesses__user=self, accesses__right_edit=True)
               | Q(accesses__user=self, accesses__right_review=True))
        ).distinct()

    @property
    def admin_accesses(self) -> QuerySet:
        try:
            from users.models import Access
        except ImportError:
            raise ImportError('users app not implemented')

        return self.accesses.filter(
            Access.is_valid_Q()
            & Q(right_review=True)
        )

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        abstract = False
