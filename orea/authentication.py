from dj_rest_auth.jwt_auth import JWTCookieAuthentication
from dj_rest_auth.utils import jwt_encode
from django.contrib.auth.backends import ModelBackend

from . import conf_auth
from .models import User
from .types import LoginError, JwtTokens


class AuthBackend(ModelBackend):
    """
    Used for login and logout
    """
    def authenticate(self, request, username=None, password=None, **kwargs):
        """
        Must return the user from login form if password is correct
        After setting request.jwt_session_key and request.jwt_refresh_key
        """
        try:
            tokens: JwtTokens = conf_auth.check_ids(username=username,
                                                    password=password)
        except NotImplementedError:
            user = super(AuthBackend, self).authenticate(
                request, username=username, password=password, **kwargs
            )
            request.jwt_session_key, request.jwt_refresh_key = jwt_encode(user)
            return user
        except LoginError:
            return None
        else:
            request.jwt_session_key = tokens.access
            request.jwt_refresh_key = tokens.refresh
            request.last_connection = tokens.last_connection

            try:
                user = conf_auth.get_user(tokens.access)
            except NotImplementedError:
                try:
                    user = User.objects.get(username=username)
                except User.DoesNotExist:
                    return None
            return user


class CustomAuthentication(JWTCookieAuthentication):
    def get_validated_token(self, raw_token):
        """
        Validates an encoded JSON web token and returns a validated token
        wrapper object.
        Token returned will be used with get_user
        """
        try:
            return conf_auth.validate_token(raw_token)
        except NotImplementedError:
            return super(CustomAuthentication, self)\
                .get_validated_token(raw_token)

    def get_user(self, validated_token):
        try:
            return conf_auth.get_user(validated_token)
        except NotImplementedError:
            return super(CustomAuthentication, self).get_user(validated_token)
