from django.contrib import admin
from .models import Access


class AccessAdmin(admin.ModelAdmin):
    pass


admin.site.register(Access, AccessAdmin)
