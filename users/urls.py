from django.urls import path, include

from orea.urls import OptionalSlashRouter
from . import views

router = OptionalSlashRouter(trailing_slash=False)
router.register(r'accesses', views.AccessViewSet, basename="accesses")

urlpatterns = [
    path('', include(router.urls)),
]
