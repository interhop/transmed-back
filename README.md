<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#developer-guidelines">Developer guidelines</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#testing">Testing</a></li>
    <li><a href="#data-model">Data model</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Orea will to facilitate the share of patient condition between caregivers within intensive care units.

### Built With

Here is a list of major frameworks used in this project
* [Django](https://www.djangoproject.com)
* [Django REST Framework](https://www.django-rest-framework.org/)
* [PosgreSQL](https://www.postgresql.org/)

Many thanks to other open source valuable modules:
* [drf-extensions]()
* [django-filter]()
* [djangorestframework_simplejwt]()
* [drf-spectacular (Swagger Generator)](https://drf-spectacular.readthedocs.io/en/latest/)
* [drf-api-tracking]()
* [django-safedelete]()
* [djangorestframework-camel-case]()
* [coverage]()
* [nltk]()
* [pyjwt]()
* [names]()


<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

* Python
  ```sh
  sudo apt-get update
  sudo apt-get install python3.10
  ```
* PostgreSQL
  ```sh
  sudo apt-get install gcc postgresql postgresql-contrib
  ```

### Installation

3 possibilities :
- locally
- locally using docker-compose for a demo
- official setup using docker-compose

#### Locally

1. Clone the repo
   ```sh
   git clone https://framagit.org/interhop/orea/back-end.git
   cd back-end
   ```
2. Prepare a virtual environment
   ```sh
   pip install virtualenv
   virtualenv -p python3.8 venv
   source venv/bin/activate
   pip install -r requirements.txt
   ```
3. Prepare your database
   ```sh
   sudo -u postgres psql
   ```
   ```psql
   CREATE USER orea_dev PASSWORD 'password';
   CREATE DATABASE orea_dev OWNER orea_dev;
   \q
   ```
4. Configuration : 
- Create your .env file in folder `orea` using `orea/example.env`
- Modify your orea/settings.py file if necessary
- Modify your orea/conf_auth.py file if necessary

5. Now run Django migrations in that order
   ```sh
   source venv/bin/activate
   python manage.py migrate
   ```
6. In order to allow Django to run its tests, authorise the user to create a test database
  ```sh
    sudo -u postgres psql
  ```
  ```psql
  ALTER USER orea_dev CREATEDB;  
  ```
7. If you want to run the server locally, you'll need to give your user access to the schemas
  ```pqsl
    \c orea_dev
    GRANT ALL PRIVILEGES ON DATABASE orea_dev TO orea_dev;
  ```
8. If you want to create a small basic Database to run a demo, two ways :
- first, creating an admin user called *ckramer* : 
```commandline
python manage.py reset_demo
```

- or run the following few lines

```python
from infra.models import *
from patients.models import *
from orea.models import *
from users.models import *

u = User.objects.create_superuser(username="ckramer", first_name="Cid", last_name="Kramer", email="cid.kramer@garden.blb")
hosp = Hospital.objects.create(name='Balamb')
rea_serv = IntensiveCareService.objects.create(name="Seed", hospital=hosp)
unit_1 = Unit.objects.create(name="Squall", intensive_care_service=rea_serv)
unit_2 = Unit.objects.create(name="Linoa", intensive_care_service=rea_serv)
beds_1 = Bed.objects.bulk_create(Bed(unit=unit_1, unit_index=i) for i in range(5))
beds_2 = Bed.objects.bulk_create(Bed(unit=unit_2, unit_index=i) for i in range(5))
Access.objects.create(
    user=u, intensive_care_service=rea_serv, start_datetime=timezone.now(),
    end_datetime=timezone.now() + timedelta(days=182),
    status=Access.Status.validated, reviewed_by=u, right_edit=True,
    right_review=True
)
```

#### Locally using docker-compose

First, install *docker* and *docker-compose* :
```commandline
sudo apt install docker docker-compose
```

Then, from the main folder:

```commandline
docker-compose up --build
```

#### Official setup using docker-compose

On your machine, have also *docker* and *docker-compose* installed.

Copy and update files in `docker/example_setup` with files you want for replacing base setup.

Remember to update services' `ports` variables in *docker-compose*.

If your database runs on the machine hosting docker compose, consider modifying :
- variable `listen_address` (to '*' for example) from */etc/postgresql/{pg_version}/main/postgresql.conf*
- IPv4 local connections line from */etc/postgresql/{pg_version}/main/pg_hba.conf*

Else, consider updating `host-gateway` in *docker-compose.yaml* with the ip address of the database location.

And run :

```commandline
chmod +x example.docker-entrypoint.sh
docker-compose up --build
```

#### Customed authentication

The basic process of authentication is using JWT standard within cookies.

To use this, it's only necessary to reproduce _orea/example.conf_auth.py_

In order to redirect authentication process to a dedicated, external server, make the necessary requests in this file.
In this case, you can prevent local management of accounts (registration, password updating, etc.) by setting `ALLOW_ACCOUNT_MANAGEMENT = False` in the settings file.


### Developer guidelines

Before pushing online and requesting for merge request, run :
```
flake8 infra users orea patients voting  --max-line-length 81 --extend-exclude='*/conf_*','*/migrations/*'
```

<!-- USAGE EXAMPLES -->
## Usage

If you want to use it fully locally, update `orea/AuthMiddleware.py` file.

Run the server to start making request via `localhost:8000`:
```sh
source venv/bin/activate
python manage.py runserver
```
You can now go on website `localhost:8000/docs/` for more details on the API.

## Testing

Run: `python manage.py test`

<!-- MODELS -->
## Data model

![Data model](./models.png)

### By app

#### infra

![infra model](./infra/models.png)

#### patients

![patients model](./patients/models.png)

#### users

![users model](./users/models.png)

#### Regenrating models :

Add '"django_extensions"' in orea/settings.INSTALLED_APPS

```shell
pip install pydotplus
sudo apt-get install graphviz
python manage.py graph_models -a -g -o models.png --exclude-models=LogEntry,Group,Permission,PersonalNote,APIRequestLog,ContentType,Session,AbstractBaseSession,BaseAPIRequestLog,AbstractUser,AbstractBaseUser,PermissionsMixin
```


<!-- CONTRIBUTING -->
## Contributing

1. Clone the Project
2. Checkout to develop branch (`git checkout develop`)
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. (optional) In commit message, add a line `interhop/orea/issues#{issueIid}`
4. Push to the Branch (`git push -u origin feature/AmazingFeature`)
5. Open a Pull Request with develop branch


<!-- CONTACT -->
## Contact

Alexandre Martin, main contributor - [@w848](https://framagit.org/w848) - alexandre.gsm.martin@tutanota.com
